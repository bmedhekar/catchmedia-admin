$(".cm-menu-overlay").click(function() {
    $(this).removeClass("active");
    $('.lyt-nav').removeClass('hide-text');
    $(".cm-menu-overlay").removeClass("notify-overlay");
    $('.lyt-nav, .lyt-notification, .hamburger').removeClass('active');
    //$('.mobile-menu-btn').removeClass('active');
});


function gridSwitch() {
    $('.grid3').on('click', function() {
        $(".modal-tab ul li").removeClass('active');
        $(this).parent().addClass('active');
        $('.typ-reports-comparision ul li').each(function(index) {
            // console.log(index);
            $(this).removeClass('col-md-6 col-md-12').addClass('col-md-4');
        });
        redrawOnModal();
    });
    $('.grid2').on('click', function() {
        $(".modal-tab ul li").removeClass('active');
        $(this).parent().addClass('active');
        $('.typ-reports-comparision ul li').each(function(index) {
            // console.log(index);
            $(this).removeClass('col-md-4 col-md-12').addClass('col-md-6');
        });
        redrawOnModal();
    });
    // $('.grid1').on('click', function() {
    //     $(".modal-tab ul li").removeClass('active');
    //     $(this).parent().addClass('active');
    //     $('.typ-reports-comparision ul li').each(function(index) {
    //         // console.log(index);
    //         $(this).removeClass('col-md-4 col-md-6').addClass('col-md-12');
    //     });
    //     redrawOnModal();
    // });
}


$(function() {
    $("a").each(function() {
        if ($(this).attr("href") == "#" || $(this).attr("href") == " ") {
            $(this).attr("href", "javascript:void(0)");
        }
    });

    $('.js-notification').on('click', function() {
        $('.lyt-notification, .cm-menu-overlay').toggleClass('active');
        $('.cm-menu-overlay').toggleClass('notify-overlay');

        $(".mod-filter-opt, .mod-timeframe-opt, .tag-hover-filter, .lyt-nav, .main, .hamburger").removeClass("active");
        clearTimeout(timerVar);
        $("body").removeClass("overlay-opened");
        $(".lyt-nav").removeClass("hide-text");
        $(".lyt-nav").removeClass("expand");
        $(".lyt-nav").removeClass("active");
        $("body").removeClass("overlay-opened");
    });


    var swiper1 = new Swiper('.swiper-slider1', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        speed: 600,
        spaceBetween: 1,
        initialSlide: 0,
        centeredSlides: false,
        paginationClickable: true,
        simulateTouch: false,
        breakpoints: {
            640: {
                simulateTouch: true,
                slidesPerView: 1,
                initialSlide: 1
            },
            920: {
                slidesPerView: 1,
                initialSlide: 0,
                centeredSlides: false
            },
            1200: {
                slidesPerView: 2,
                initialSlide: 0,
                centeredSlides: false,
            },
            1599: {
                slidesPerView: 3,
                initialSlide: 0,
                centeredSlides: false,
            },
        }
    });
    var swiper2 = new Swiper('.swiper-slider2', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        speed: 600,
        spaceBetween: 1,
        initialSlide: 0,
        centeredSlides: false,
        paginationClickable: true,
        simulateTouch: false,
        breakpoints: {
            640: {
                simulateTouch: true,
                slidesPerView: 1,
                initialSlide: 1
            },
            920: {
                slidesPerView: 1,
                initialSlide: 0,
                centeredSlides: false
            },
            1200: {
                slidesPerView: 2,
                initialSlide: 0,
                centeredSlides: false,
            },
            1599: {
                slidesPerView: 3,
                initialSlide: 0,
                centeredSlides: false,
            },
        }
    });
    var swiper3 = new Swiper('.swiper-slider3', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        speed: 600,
        spaceBetween: 1,
        initialSlide: 0,
        centeredSlides: false,
        paginationClickable: true,
        simulateTouch: false,
        breakpoints: {
            640: {
                simulateTouch: true,
                slidesPerView: 1,
                initialSlide: 1
            },
            920: {
                slidesPerView: 1,
                initialSlide: 0,
                centeredSlides: false
            },
            1200: {
                slidesPerView: 2,
                initialSlide: 0,
                centeredSlides: false,
            },
            1599: {
                slidesPerView: 3,
                initialSlide: 0,
                centeredSlides: false,
            },
        }
    });

    ///swiper for tabs in audience page
    var swiper10 = new Swiper('.tab-specification-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        speed: 600,
        simulateTouch: false,
        observer: true,
        observeParents: true,
        spaceBetween: 0,
        noSwiping: true,
        noSwipingClass: "swiper-no-swiping",
        onSlideChangeEnd: function(swiper) {
            var winwidth2 = $(window).width();
            if (winwidth2 < 768) {
                var activeSlide = swiper.activeIndex;
                $(".tab-specification-slider").find(".swiper-slide").removeClass("active");
                var attrHref = $(".tab-specification-slider").find(".swiper-slide").eq(activeSlide).find("a").attr("href");
                $("#myTabs li a[href='" + attrHref + "']").trigger("click");
            }
        },

        breakpoints: {
            1024: {
                slidesPerView: 2
            },
            768: {
                slidesPerView: 1
            }
        }
    });

    swiper9 = new Swiper('.compose-tab-swiper', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 2,
        speed: 600,
        spaceBetween: 0
    });


    $('#kpi-preview').on('shown.bs.modal', function() {
        swiper1.update();
    });

    $('#kpi-preview2').on('shown.bs.modal', function() {
        swiper2.update();

    });
    $('#kpi-preview3').on('shown.bs.modal', function() {
        swiper3.update();
    });

    $(".hide-show-sys-option").on("click", function() {
        var $this = $(this);
        $this.parents(".lyt-system-card").find(".card-list-wrap").toggle();
        $this.toggleClass('change');
        if ($this.hasClass('change')) {
            $this.html('<span class="icon icon-plus-bold"> Show System</span>');
        } else {
            $this.html('<span class="icon icon-minus-bold"> Hide System</span>');
        }
    });

    $('.btn-flip').click(function() {
        $(this).toggleClass('active');
        $(this).parents(".flip-wrap").find('.flip').toggleClass('active');
    });

    setTimeout(function() {
        $(".cm-loader").addClass("hide");
    }, 2000);


    $('.mod-checkbox-list input[type="checkbox"]').click(function() {
        $(this).closest("li").find(".mod-checkbox-list").toggleClass("active");
        $(this).closest("li").find(".mod-checkbox-list").find('input[type="checkbox"]').prop('checked', false);
    });

    $(".toggle-head input[type='checkbox']").click(function() {
        var checked_status = $(this).prop('checked');
        if (checked_status == true) {
            $(this).closest(".mod-toggle-list").children("ul").find('input[type="checkbox"]').prop('checked', true);
        }
        if (checked_status == false) {
            $(this).closest(".mod-toggle-list").children("ul").find('input[type="checkbox"]').prop('checked', false);
        }
    });

    $(".filter-btn").click(function() {
        var isOpened = $(this).parent(".dropdown").hasClass("open");
        var checkAppliedFilter = $(this).closest(".filter-wrap").siblings(".filters-applied").length;
        //alert(checkAppliedFilter);
        if (isOpened && checkAppliedFilter) {
            $(this).closest(".filter-wrap").siblings(".filters-applied").show();
            $(".filter-wrap").removeClass("active");
        } else {
            $(this).closest(".filter-wrap").siblings(".filters-applied").hide();
            $(this).closest(".filter-wrap").addClass("active");
        }
    });

    $(".filter-wrap .dropdown-menu *").click(function(e) {
        e.stopPropagation();
    });

    $(document).mouseup(function(e) {
        var container = $(".filter-wrap .dropdown-menu");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $(".filter-wrap").removeClass("active");
        }
    });


    // $('.mod-checkbox-list a[data-toggle="collapse"]').click(function() {
    //     $(".accord-head").removeClass("accord-opned");
    //     $(this).closest(".accord-head").addClass("accord-opned");
    // });

    $('.typ-add-kpi .switch input').change(function() {
        if ($(this).is(":checked")) {
            $(this).parents(".typ-add-kpi").find(".create-new").addClass("active");
            $(this).parents(".modal-body").addClass("add-kpi-height");
        } else {
            $(this).parents(".typ-add-kpi").find(".create-new").removeClass("active");
            $(this).parents(".modal-body").removeClass("add-kpi-height");
        }
    });


    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 80) {
            $(".mod-left-nav").addClass("active");
        } else {
            $(".mod-left-nav").removeClass("active");
        }
    });


    $(".btn-search").on("click", function() {
        $(this).closest(".bs-search").toggleClass("active");
    });

    $(".mod-filter .btn-more").on("click", function() {
        if (!$(this).hasClass("btn-less")) {
            $(this).addClass("btn-less");
            $(this).closest(".mod-filter").addClass("show-more");
        } else {
            $(this).removeClass("btn-less");
            $(this).closest(".mod-filter").removeClass("show-more");

        }
    });


    $(".mod-filter .title").on("click", function() {
        $(".lyt-notification, .mod-timeframe-opt, .tag-hover-filter, .lyt-nav, .main, .hamburger").removeClass("active");
        var $this = $(this);

        if (!$(".mod-filter-opt").hasClass("active")) {
            $(".cm-overlay2, .mod-filter-opt").addClass("active");
            $("body").addClass("overlay-opened");
        } else {
            $(".cm-overlay2, .mod-filter-opt").removeClass("active");
            $("body").removeClass("overlay-opened");
        }
        var filterHeight = $(".main > .lyt-filter").outerHeight();
        var checkthis = $(this).closest(".lyt-filter").hasClass("fixed-filter");
        // console.log(checkthis);
        if (checkthis == false) {

            $("html").animate({
                scrollTop: (filterTopPos - headerHeight) + "px"
            });

            timerVar = setTimeout(() => {
                $("body").css({ "margin-top": filterHeight + "px" });
                $(".main > .lyt-filter").addClass("fixed-filter");
                $(".main > .lyt-filter").css({ top: headerHeight });
            }, 500);

        }
    });

    $(".lyt-filter .lvl1 > .tabs .nav-tabs .item").on("click", function() {
        if (!$(this).hasClass("active")) {
            $(".lyt-filter .lvl1 > .tabs .tab-pane .filter-list .item").removeClass("active");
            $(".typ-with-filter").removeClass("active");
        }
    });


    $(".lyt-filter .lvl1 .filter-list .item").on("click", function() {
        $(".lyt-filter .lvl1 .filter-list .item").removeClass("active");
        $(this).addClass("active");
        var clickedLI = $(this).attr("name");
        var activeTab = $(this).closest(".tab-pane").attr("id");
        $(".tab-sub-options").removeClass("active");
        if (typeof clickedLI !== typeof undefined && clickedLI !== false) {
            $(".tab-sub-options." + clickedLI).addClass("active");
        } else {
            $(".tab-sub-options." + activeTab).addClass("active");
        }
        $(".lvl2").addClass("active");

        if ($('.lvl2').hasClass('active')) {
            $('.filter-timeframe-title').hide();
        }
    });

    $(".main  .lyt-filter .mod-filter .lyt-tag .bs-tag .num, .main .lyt-filter .mod-filter .lyt-tag .bs-tag .text").click(function() {
        $(".lyt-notification, .cm-menu-overlay, .mod-timeframe-opt, .mod-filter-opt,  .lyt-nav, .main, .hamburger").removeClass("active");
        $(".cm-menu-overlay").removeClass("notify-overlay");
        $("body").addClass("overlay-opened");
        $(".tag-hover-filter, .cm-overlay2").addClass("active");
        var currentPos = $(this).closest(".bs-tag").offset().left;

        if ($(this).closest(".lyt-graph").length) {
            $(".tag-hover-filter").css("left", (currentPos - 23) + "px");
        } else {
            //console.log(34);
            $(".tag-hover-filter").css("left", currentPos + "px");
        }
    });

    $(".more-tag").on("click", function() {
        $(this).toggleClass("active");
        $(this).parents(".mod-tag").find(".filter-list").toggleClass("active");
    });


    $('#myModal-graph-detail').on('shown.bs.modal', function() {
        redrawOnModal();
    });

    $('.btn-rearrange').click(function() {
        $('.sec-cont .typ-reports-comparision').toggleClass('active');
        $('.modal-tab').toggleClass('disable');
        // if ($('.sec-cont .typ-reports-comparision').hasClass('active')) {
        //     $('.sec-cont .typ-reports-comparision .list .item').removeClass('col-md-6');
        //     $('.sec-cont .typ-reports-comparision .list .item').removeClass('col-md-4');
        //     $('.sec-cont .typ-reports-comparision .list .item').addClass('col-md-3');
        // } else {
        //     $('.sec-cont .typ-reports-comparision .list .item').removeClass('col-md-3');
        //     $('.sec-cont .typ-reports-comparision .list .item').addClass('col-md-6');
        // }
        redrawOnModal();
    });


    if ($('#myModal-graph-detail').length) {
        gridSwitch();
    }


    var winH = $(window).outerHeight();
    var headerH = $('.bs-header').outerHeight();
    var breadcrumbH = $('.bs-breadcrumb').outerHeight();
    var summaryH = $('.lyt-top-bar').outerHeight();
    var footerH = $('.bs-footer').outerHeight();
    var audStatusH = $('.mod-audience-status.active').outerHeight();
    var fullH = winH - (headerH + breadcrumbH + summaryH + footerH + audStatusH);
    if ($('.lyt-rule').length != 0) {
        $('.lyt-rule').css('height', fullH);
    }



    var ruleHtml = $(".rule-wrapper .wrapper-title").html();
    var graphHtml = $(".graph-wrapper .wrapper-title").html();
    var wrapperDivOpen = "<div class='title-wrap'>";
    var wrapperDivclose = "</div>";
    var HTMLContent = wrapperDivOpen + "<div class='rule-heading'>" + ruleHtml + "</div><div class='graph-heading'>" + graphHtml + "</div>" + wrapperDivclose;
    $(HTMLContent).insertAfter(".rule-aside .aside-head");


    $('.js-expand').click(function() {
        if ($(this).hasClass("collapsed")) {
            $(this).find(".icon").removeClass("icon-collapse").addClass("icon-expand");
            $(this).find(".text").text("expand view");
            $(this).removeClass("collapsed");
            $('.accord-wrap .accord-head a').addClass('collapsed');
            // $('.accord-wrap .accord-head a').removeClass('active');
            $('.accord-wrap .accord-body').removeClass('in');
            $('.accord-wrap .accord-head a').attr("aria-expanded", "false");
        } else {
            $(this).addClass("collapsed");
            $(this).find(".icon").removeClass("icon-expand").addClass("icon-collapse");
            $(this).find(".text").text("Collapsed view");
            $('.accord-wrap .accord-head a').removeClass('collapsed');
            $('.accord-wrap .accord-head a').attr("aria-expanded", "true");

            // $('.accord-wrap .accord-head a').addClass('active');
            $('.accord-wrap .accord-body').addClass('in');
        }

    });


    $('.panel-collapse').on('shown.bs.collapse', function(e) {
        var $panel = $(this).closest('.panel');
        $('html,body').animate({
            scrollTop: $panel.offset().top - 70
        }, 500);
    });

    $(".js-change-view").on("click", function() {
        if ($(".event-list").hasClass("listview")) {
            $(".event-list").removeClass("listview");
            $(".event-list").addClass("defaultview");
        } else {
            $(".event-list").addClass("listview");
            $(".event-list").removeClass("defaultview");
        }
    });


    if ($('.decision-tree-wrap').length) {
        var matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/,
            matches = $('.decision-tree-wrap').css('-webkit-transform').match(matrixRegex);
        console.log(matches);
        var incr = 0.1;
        $('.btn-zoom.plus').click(function() {
            if (matches[1] < 1) {
                matches[1] += incr;
                $('.decision-tree-wrap > ul.node').css({
                    '-webkit-transform': 'scale(' + matches[1] + ')'
                });
            }
        });

        $('.btn-zoom.minus').click(function() {
            if (matches[1] > 0.5) {
                matches[1] -= incr;
                $('.decision-tree-wrap > ul.node').css({
                    '-webkit-transform': 'scale(' + matches[1] + ')'
                });
            }
        });

    }


    $(document).on("click", ".mod-language .btn-more", function() {
        $(this).parents(".mod-language").addClass("show-more");
        $(this).addClass("btn-less");
        $(this).removeClass("btn-more");

    });
    $(document).on("click", ".mod-language .btn-less", function() {
        $(this).parents(".mod-language").removeClass("show-more");
        $(this).removeClass("btn-less");
        $(this).addClass("btn-more");
    });

    $('.switch input').change(function() {
        //console.log();
        var checkswitch = $(this).is(":checked")
        if ($(this).is(":checked")) {
            if ($(this).parents(".ab-testing")) {
                $(".ab-testing").addClass("active");
            }
            $(this).closest(".swipe-radio-container").find(".create-new").addClass("active");
        } else {
            if ($(this).parents(".ab-testing")) {
                $(".ab-testing").removeClass("active");
            }
            $(this).closest(".swipe-radio-container").find(".create-new").removeClass("active");
        }
    });



    $("a[data-toggle='dropdown']").click(function() {
        $(".notify-overlay,.lyt-notification").removeClass("active");
    });


});