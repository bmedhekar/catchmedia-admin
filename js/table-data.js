var dashboardDefs = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,

}, {
    field: 'Status',
    width: 140,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Dashboard Name',
    width: 200,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Type',
    width: 130,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Reports',
    width: 135,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Suggested Campaigns',
    width: 250,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'KPIs',
    width: 150,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, ];
var dashboardGridOptions = {
    suppressRowClickSelection: true,
    columnDefs: dashboardDefs,
    rowHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    //rowData: rowData,
    rowSelection: 'multiple',
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function statusCellRenderer() {}

statusCellRenderer.prototype.init = function(params) {

    this.eGui = document.createElement('span');
    this.eGui.innerHTML = '<span class="' + params.data.Status + '">' + params.data.Status + '</span>';
};

statusCellRenderer.prototype.getGui = function() {
    return this.eGui;
};



function myinit(gridDiv) {
    new agGrid.Grid(gridDiv, dashboardGridOptions);

    agGrid.simpleHttpRequest({
        url: '../../assets/admin-table-data.json'
    }).then(function(data) {

        dashboardGridOptions.api.setRowData(data);
    });
}
if ($("#dashboard").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#dashboard');
        myinit(gridDiv);
    });
}


var managelistviewColumnDefs2 = [{
    field: 'Status',
    width: 100,
    suppressFilter: true,
    cellRenderer: 'statusCellRenderer',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Channel Partner Name',
    width: 300,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Vertical Type',
    width: 228,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'System Integrators',
    width: 200,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'CP Direct Customers',
    width: 200,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Date Created',
    width: 200,
    sortingOrder: ['asc', 'desc']
}];
var managelistviewGridOptions2 = {

    suppressRowClickSelection: true,
    columnDefs: managelistviewColumnDefs2,
    enableSorting: true,
    rowHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinit2managelistview(gridDiv) {
    new agGrid.Grid(gridDiv, managelistviewGridOptions2);

    agGrid.simpleHttpRequest({
        url: '../../assets/admin-table-data2.json'
    }).then(function(data) {

        managelistviewGridOptions2.api.setRowData(data);
    });
}

if ($("#managelistview").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv1 = document.querySelector('#managelistview');
        myinit2managelistview(gridDiv1);
    });
}


var managelistview2ColumnDefs2 = [{
    field: 'Status',
    width: 100,
    suppressFilter: true,
    cellRenderer: 'statusCellRenderer',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Channel Partner Name',
    width: 300,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Vertical Type',
    width: 228,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'System Integrators',
    width: 200,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'CP Direct Customers',
    width: 200,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Date Created',
    width: 200,
    sortingOrder: ['asc', 'desc']
}];
var managelistview2GridOptions2 = {

    suppressRowClickSelection: true,
    columnDefs: managelistview2ColumnDefs2,
    enableSorting: true,
    rowHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinit2managelistview2(gridDiv) {
    new agGrid.Grid(gridDiv, managelistview2GridOptions2);

    agGrid.simpleHttpRequest({
        url: '../../assets/admin-table-data2.json'
    }).then(function(data) {

        managelistview2GridOptions2.api.setRowData(data);
    });
}
if ($("#managelistview2").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv1 = document.querySelector('#managelistview2');
        myinit2managelistview2(gridDiv1);
    });
}


// manage persona start

var managepersona = [{
    headerName: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    cellClass: 'cell-wrap-text',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: 'Status',
    field: "Status",
    width: 120,
    cellClass: 'cell-wrap-text',
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']

}, {
    headerName: 'Persona Name',
    field: "Persona Name",
    width: 400,
    cellClass: 'cell-wrap-text',
    autoHeight: true,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: 'Product Type',
    field: "product_type",
    width: 450,
    cellClass: 'cell-wrap-text',
    autoHeight: true,
    filter: 'agTextColumnFilter',
    cellRenderer: 'iconCellRenderer',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: 'Customer Count',
    field: "Customer Count",
    width: 176,
    cellClass: 'cell-wrap-text',
    autoHeight: true,

    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}];
var managepersonaOption = {
    suppressRowClickSelection: true,
    columnDefs: managepersona,
    enableSorting: true,
    //rowHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,

    onGridReady: function(params) {
        setTimeout(function() {
            managepersonaOption.api.resetRowHeights();
        }, 500);
    },
    components: {
        'statusCellRenderer': statusCellRenderer,
        'iconCellRenderer': iconCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinitmanagepersona(gridDiv) {
    new agGrid.Grid(gridDiv, managepersonaOption);
    agGrid.simpleHttpRequest({
        url: '../../assets/admin-table-data4.json'
    }).then(function(data) {
        managepersonaOption.api.setRowData(data);
    });
}

function iconCellRenderer() {}

iconCellRenderer.prototype.init = function(params) {
    this.eGui = document.createElement('span');
    this.eGui.innerHTML = '<span class="' + params.data.ptype + '">' + params.data.product_type + '</span>';
};

iconCellRenderer.prototype.getGui = function() {
    return this.eGui;
};


if ($("#managepersona").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#managepersona');
        myinitmanagepersona(gridDiv);
    });
}

// manage personal end

var goalsKPI = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Goal Name',
    width: 300,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, , {
    field: 'Category',
    width: 250,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "KPIs",
    field: 'KPIs',
    width: 80,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Product Type',
    width: 300,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var goalsKPIgridOptions = {
    suppressRowClickSelection: true,
    columnDefs: goalsKPI,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};



if ($(".table-wrap").length) {
    function myinitgoalsKPI(gridDiv) {
        new agGrid.Grid(gridDiv, goalsKPIgridOptions);
        agGrid.simpleHttpRequest({
            url: '../../assets/goals-kpi.json'
        }).then(function(data) {

            goalsKPIgridOptions.api.setRowData(data);
        });
    }
}




var audience = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    suppressFilter: true,
    checkboxSelection: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    suppressFilter: true,
    cellRenderer: 'statusCellRenderer',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Audience Name',
    width: 351,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Audience Rule',
    width: 404,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var audiencegridOptions = {
    suppressRowClickSelection: true,
    columnDefs: audience,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinitaudience(gridDiv) {
    new agGrid.Grid(gridDiv, audiencegridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/audience.json'
    }).then(function(data) {

        audiencegridOptions.api.setRowData(data);
    });
}

if ($("#audience").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#audience');
        myinitaudience(gridDiv);
    });
}

// audience2 start


var audience2 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    suppressFilter: true,
    checkboxSelection: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    suppressFilter: true,
    cellRenderer: 'statusCellRenderer',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Audience Name',
    width: 351,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Audience Rule',
    width: 404,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var audience2gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: audience2,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinitaudience2(gridDiv) {
    new agGrid.Grid(gridDiv, audience2gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/audience.json'
    }).then(function(data) {

        audience2gridOptions.api.setRowData(data);
    });
}

if ($("#audience2").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#audience2');
        myinitaudience2(gridDiv);
    });
}

// audience2 end

var keyIndicators = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    cellClass: 'cell-wrap-text',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "Key Indicator Name",
    field: 'kiname',
    width: 284,
    cellRenderer: 'boldtextRenderer',
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Based on Report',
    width: 354,
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Assigned to Dashboard',
    width: 254,
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var keyIndicatorsgridOptions = {
    suppressRowClickSelection: true,
    columnDefs: keyIndicators,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer,
        'boldtextRenderer': boldtextRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }

};


function boldtextRenderer() {}

boldtextRenderer.prototype.init = function(params) {

    this.eGui = document.createElement('span');
    this.eGui.innerHTML = params.data.kiname + '<span class="cm-bold">' + params.data.boldtext + '</span>';
};

boldtextRenderer.prototype.getGui = function() {
    return this.eGui;
};


function myinitkeyIndicators(gridDiv) {
    new agGrid.Grid(gridDiv, keyIndicatorsgridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/keyIndicators.json'
    }).then(function(data) {

        keyIndicatorsgridOptions.api.setRowData(data);
    });
}


if ($("#keyIndicators").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#keyIndicators');
        myinitkeyIndicators(gridDiv);
    });
}

// ki2 start

var keyIndicators2 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    cellClass: 'cell-wrap-text',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "Key Indicator Name",
    field: 'kiname',
    width: 284,
    cellRenderer: 'boldtextRenderer',
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Based on Report',
    width: 354,
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Assigned to Dashboard',
    width: 254,
    cellClass: 'cell-wrap-text',
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var keyIndicators2gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: keyIndicators2,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer,
        'boldtextRenderer': boldtextRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};

function myinitkeyIndicators2(gridDiv) {
    new agGrid.Grid(gridDiv, keyIndicators2gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/keyIndicators.json'
    }).then(function(data) {
        keyIndicators2gridOptions.api.setRowData(data);
    });
}


if ($("#keyIndicators2").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#keyIndicators2');
        myinitkeyIndicators2(gridDiv);
    });
}

// ki2 end

var reports = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Report Name',
    width: 354,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Category',
    width: 200,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Stack Count',
    width: 150,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: "Dashboard",
    width: 200,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var reportsgridOptions = {
    suppressRowClickSelection: true,
    columnDefs: reports,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitreports(gridDiv) {
    new agGrid.Grid(gridDiv, reportsgridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/reports.json'
    }).then(function(data) {

        reportsgridOptions.api.setRowData(data);
    });
}


if ($("#reports").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#reports');
        myinitreports(gridDiv);
    });
}

// report2 start
var reports2 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Report Name',
    width: 354,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Category',
    width: 200,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Stack Count',
    width: 150,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: "Dashboard",
    width: 200,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}];
var reports2gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: reports2,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitreports2(gridDiv) {
    new agGrid.Grid(gridDiv, reports2gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/reports.json'
    }).then(function(data) {

        reports2gridOptions.api.setRowData(data);
    });
}


if ($("#reports2").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#reports2');
        myinitreports2(gridDiv);
    });
}

// report2 end

var communication = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Campaign Name',
    width: 274,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Goal Name',
    width: 365,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "KPIs",
    field: 'KPIs',
    width: 110,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}];
var communicationgridOptions = {
    suppressRowClickSelection: true,
    columnDefs: communication,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitcommunication(gridDiv) {
    new agGrid.Grid(gridDiv, communicationgridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/communication.json'
    }).then(function(data) {
        communicationgridOptions.api.setRowData(data);
    });
}


if ($("#communication").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#communication');
        myinitcommunication(gridDiv);
    });
}

// communication2 start

var communication2 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Campaign Name',
    width: 274,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Goal Name',
    width: 365,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "KPIs",
    field: 'KPIs',
    width: 110,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}];
var communication2gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: communication2,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitcommunication2(gridDiv) {
    new agGrid.Grid(gridDiv, communication2gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/communication.json'
    }).then(function(data) {

        communication2gridOptions.api.setRowData(data);
    });
}


if ($("#communication2").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#communication2');
        myinitcommunication2(gridDiv);
    });
}



// communication3 start

var communication3 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Campaign Name',
    width: 274,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Goal Name',
    width: 365,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "KPIs",
    field: 'KPIs',
    width: 110,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}];
var communication3gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: communication3,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitcommunication3(gridDiv) {
    new agGrid.Grid(gridDiv, communication3gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/communication.json'
    }).then(function(data) {

        communication3gridOptions.api.setRowData(data);
    });
}


if ($("#communication3").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#communication3');
        myinitcommunication3(gridDiv);
    });
}



// communication4 start

var communication4 = [{
    field: '',
    width: 53,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    checkboxSelection: true,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Status',
    width: 120,
    cellRenderer: 'statusCellRenderer',
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Campaign Name',
    width: 274,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    field: 'Goal Name',
    width: 365,
    filter: 'agTextColumnFilter',
    sortingOrder: ['asc', 'desc']
}, {
    headerName: "KPIs",
    field: 'KPIs',
    width: 110,
    suppressFilter: true,
    sortingOrder: ['asc', 'desc']
}];
var communication4gridOptions = {
    suppressRowClickSelection: true,
    columnDefs: communication4,
    rowHeight: 40,
    groupHeaderHeight: 40,
    enableSorting: true,
    enableFilter: true,
    floatingFiltersHeight: 52,
    headerHeight: 40,
    rowSelection: 'multiple',
    floatingFilter: true,
    components: {
        'statusCellRenderer': statusCellRenderer
    },
    // onGridReady: function(params) {
    //     params.api.sizeColumnsToFit();
    // }
};


function myinitcommunication4(gridDiv) {
    new agGrid.Grid(gridDiv, communication4gridOptions);
    agGrid.simpleHttpRequest({
        url: '../../assets/communication.json'
    }).then(function(data) {

        communication4gridOptions.api.setRowData(data);
    });
}


if ($("#communication4").length) {
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#communication4');
        myinitcommunication4(gridDiv);
    });
}



function redrawAllRows() {
    //progressColor();
    goalsKPIgridOptions.api.redrawRows();
    audiencegridOptions.api.redrawRows();
    dashboardGridOptions.api.redrawRows();
    communicationgridOptions.api.redrawRows();
    communication2gridOptions.api.redrawRows();
    communication3gridOptions.api.redrawRows();
    communication4gridOptions.api.redrawRows();
    reports2gridOptions.api.redrawRows();
    reportsgridOptions.api.redrawRows();
    keyIndicators2gridOptions.api.redrawRows();
    keyIndicatorsgridOptions.api.redrawRows();
    audience2gridOptions.api.redrawRows();
    managepersonaOption.api.redrawRows();
    managelistviewGridOptions2.api.redrawRows();
    managelistview2GridOptions2.api.redrawRows();
}

$('.tabs .nav li').click(function() {
    redrawAllRows();
});

// communication2 end