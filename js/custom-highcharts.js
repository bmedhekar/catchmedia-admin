//var graphWidth = $('.graph').width();
var legends = {
    //backgroundColor: "#fff",
    align: "left",

    //width: graphWidth,
    padding: 15,
    marginTop: 10,
    itemMarginBottom: 5,
    x: -10,
    y: 15,
    verticalAlign: 'bottom',
    itemStyle: {
        color: '#000',
        fontWeight: 'light',
        fontSize: '10px',
    }
};
var responsiveData = {
    rules: [{
        condition: {
            maxWidth: 1200
        },
        chartOptions: {
            chart: {
                height: 320
            }
        }
    }, {
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            chart: {
                height: 220
            }
        }
    }]
};
// Highcharts.setOptions({
//     colors: ['#fee276', '#bfed69', '#f08db6', '#8ce5dd', '#b186ea', '#f29a90', '#c43b99', '#89d4f0', '#e39969', '#86a0b9'],
// });

Highcharts.setOptions({
    colors: ['#a191fe', '#74d3ff', '#9ce6cf', '#ffdd67', '#ff679a', '#ff9c9c', '#7568f4', '#31aee8', '#50c9ba', '#ffcd38', '#c54c82', '#f93f40', '#543fe4', '#007ad8', '#4ba2ac', '#ffa800', '#bd0859', '#b02221'],
});

var colReverse = (function() {
    var colorreverse = [];
    var i;
    for (i = Highcharts.getOptions().colors.length - 1; i >= 0; i--) {
        colorreverse.push(Highcharts.getOptions().colors[i]);
    }
    return colorreverse;
}());
//console.log(colReverse);

var data = [{
        'id': '0.0',
        'parent': '',
        'name': 'The World'
    }, {
        'id': '1.3',
        'parent': '0.0',
        'name': 'Asia'
    }, {
        'id': '1.1',
        'parent': '0.0',
        'name': 'Africa'
    }, {
        'id': '1.2',
        'parent': '0.0',
        'name': 'America'
    }, {
        'id': '1.4',
        'parent': '0.0',
        'name': 'Europe'
    }, {
        'id': '1.5',
        'parent': '0.0',
        'name': 'Oceanic'
    },

    /* Africa */
    {
        'id': '2.1',
        'parent': '1.1',
        'name': 'Eastern Africa'
    },

    {
        'id': '3.1',
        'parent': '2.1',
        'name': 'Ethiopia',
        'value': 104957438
    }, {
        'id': '3.2',
        'parent': '2.1',
        'name': 'Tanzania',
        'value': 57310019
    }, {
        'id': '3.3',
        'parent': '2.1',
        'name': 'Kenya',
        'value': 49699862
    }, {
        'id': '3.4',
        'parent': '2.1',
        'name': 'Uganda',
        'value': 42862958
    }, {
        'id': '3.5',
        'parent': '2.1',
        'name': 'Mozambique',
        'value': 29668834
    }, {
        'id': '3.6',
        'parent': '2.1',
        'name': 'Madagascar',
        'value': 25570895
    }, {
        'id': '3.7',
        'parent': '2.1',
        'name': 'Malawi',
        'value': 18622104
    }, {
        'id': '3.8',
        'parent': '2.1',
        'name': 'Zambia',
        'value': 17094130
    }, {
        'id': '3.9',
        'parent': '2.1',
        'name': 'Zimbabwe',
        'value': 16529904
    }, {
        'id': '3.10',
        'parent': '2.1',
        'name': 'Somalia',
        'value': 14742523
    }, {
        'id': '3.11',
        'parent': '2.1',
        'name': 'South Sudan',
        'value': 12575714
    }, {
        'id': '3.12',
        'parent': '2.1',
        'name': 'Rwanda',
        'value': 12208407
    }, {
        'id': '3.13',
        'parent': '2.1',
        'name': 'Burundi',
        'value': 10864245
    }, {
        'id': '3.14',
        'parent': '2.1',
        'name': 'Eritrea',
        'value': 5068831
    }, {
        'id': '3.15',
        'parent': '2.1',
        'name': 'Mauritius',
        'value': 1265138
    }, {
        'id': '3.16',
        'parent': '2.1',
        'name': 'Djibouti',
        'value': 956985
    }, {
        'id': '3.17',
        'parent': '2.1',
        'name': 'Réunion',
        'value': 876562
    }, {
        'id': '3.18',
        'parent': '2.1',
        'name': 'Comoros',
        'value': 813912
    }, {
        'id': '3.19',
        'parent': '2.1',
        'name': 'Mayotte',
        'value': 253045
    }, {
        'id': '3.20',
        'parent': '2.1',
        'name': 'Seychelles',
        'value': 94737
    },

    {
        'id': '2.5',
        'parent': '1.1',
        'name': 'Western Africa'
    },

    {
        'id': '3.42',
        'parent': '2.5',
        'name': 'Nigeria',
        'value': 190886311
    }, {
        'id': '3.43',
        'parent': '2.5',
        'name': 'Ghana',
        'value': 28833629
    }, {
        'id': '3.44',
        'parent': '2.5',
        'name': 'Côte Ivoire',
        'value': 24294750
    }, {
        'id': '3.45',
        'parent': '2.5',
        'name': 'Niger',
        'value': 21477348
    }, {
        'id': '3.46',
        'parent': '2.5',
        'name': 'Burkina Faso',
        'value': 19193382
    }, {
        'id': '3.47',
        'parent': '2.5',
        'name': 'Mali',
        'value': 18541980
    }, {
        'id': '3.48',
        'parent': '2.5',
        'name': 'Senegal',
        'value': 15850567
    }, {
        'id': '3.49',
        'parent': '2.5',
        'name': 'Guinea',
        'value': 12717176
    }, {
        'id': '3.50',
        'parent': '2.5',
        'name': 'Benin',
        'value': 11175692
    }, {
        'id': '3.51',
        'parent': '2.5',
        'name': 'Togo',
        'value': 7797694
    }, {
        'id': '3.52',
        'parent': '2.5',
        'name': 'Sierra Leone',
        'value': 7557212
    }, {
        'id': '3.53',
        'parent': '2.5',
        'name': 'Liberia',
        'value': 4731906
    }, {
        'id': '3.54',
        'parent': '2.5',
        'name': 'Mauritania',
        'value': 4420184
    }, {
        'id': '3.55',
        'parent': '2.5',
        'name': 'The Gambia',
        'value': 2100568
    }, {
        'id': '3.56',
        'parent': '2.5',
        'name': 'Guinea-Bissau',
        'value': 1861283
    }, {
        'id': '3.57',
        'parent': '2.5',
        'name': 'Cabo Verde',
        'value': 546388
    }, {
        'id': '3.58',
        'parent': '2.5',
        'name': 'Saint Helena, Ascension and Tristan da Cunha',
        'value': 4049
    },

    {
        'id': '2.3',
        'parent': '1.1',
        'name': 'North Africa'
    },

    {
        'id': '3.30',
        'parent': '2.3',
        'name': 'Egypt',
        'value': 97553151
    }, {
        'id': '3.31',
        'parent': '2.3',
        'name': 'Algeria',
        'value': 41318142
    }, {
        'id': '3.32',
        'parent': '2.3',
        'name': 'Sudan',
        'value': 40533330
    }, {
        'id': '3.33',
        'parent': '2.3',
        'name': 'Morocco',
        'value': 35739580
    }, {
        'id': '3.34',
        'parent': '2.3',
        'name': 'Tunisia',
        'value': 11532127
    }, {
        'id': '3.35',
        'parent': '2.3',
        'name': 'Libya',
        'value': 6374616
    }, {
        'id': '3.36',
        'parent': '2.3',
        'name': 'Western Sahara',
        'value': 552628
    },

    {
        'id': '2.2',
        'parent': '1.1',
        'name': 'Central Africa'
    },

    {
        'id': '3.21',
        'parent': '2.2',
        'name': 'Democratic Republic of the Congo',
        'value': 81339988
    }, {
        'id': '3.22',
        'parent': '2.2',
        'name': 'Angola',
        'value': 29784193
    }, {
        'id': '3.23',
        'parent': '2.2',
        'name': 'Cameroon',
        'value': 24053727
    }, {
        'id': '3.24',
        'parent': '2.2',
        'name': 'Chad',
        'value': 14899994
    }, {
        'id': '3.25',
        'parent': '2.2',
        'name': 'Congo',
        'value': 5260750
    }, {
        'id': '3.26',
        'parent': '2.2',
        'name': 'Central African Republic',
        'value': 4659080
    }, {
        'id': '3.27',
        'parent': '2.2',
        'name': 'Gabon',
        'value': 2025137
    }, {
        'id': '3.28',
        'parent': '2.2',
        'name': 'Equatorial Guinea',
        'value': 1267689
    }, {
        'id': '3.29',
        'parent': '2.2',
        'name': 'Sao Tome and Principe',
        'value': 204327
    },

    {
        'id': '2.4',
        'parent': '1.1',
        'name': 'South America'
    },

    {
        'id': '3.37',
        'parent': '2.4',
        'name': 'South Africa',
        'value': 56717156
    }, {
        'id': '3.38',
        'parent': '2.4',
        'name': 'Namibia',
        'value': 2533794
    }, {
        'id': '3.39',
        'parent': '2.4',
        'name': 'Botswana',
        'value': 2291661
    }, {
        'id': '3.40',
        'parent': '2.4',
        'name': 'Lesotho',
        'value': 2233339
    }, {
        'id': '3.41',
        'parent': '2.4',
        'name': 'Swaziland',
        'value': 1367254
    },

    /***********/

    /* America */
    {
        'id': '2.9',
        'parent': '1.2',
        'name': 'South America'
    },

    {
        'id': '3.98',
        'parent': '2.9',
        'name': 'Brazil',
        'value': 209288278
    }, {
        'id': '3.99',
        'parent': '2.9',
        'name': 'Colombia',
        'value': 49065615
    }, {
        'id': '3.100',
        'parent': '2.9',
        'name': 'Argentina',
        'value': 44271041
    }, {
        'id': '3.101',
        'parent': '2.9',
        'name': 'Peru',
        'value': 32165485
    }, {
        'id': '3.102',
        'parent': '2.9',
        'name': 'Venezuela',
        'value': 31977065
    }, {
        'id': '3.103',
        'parent': '2.9',
        'name': 'Chile',
        'value': 18054726
    }, {
        'id': '3.104',
        'parent': '2.9',
        'name': 'Ecuador',
        'value': 16624858
    }, {
        'id': '3.105',
        'parent': '2.9',
        'name': 'Bolivia',
        'value': 11051600
    }, {
        'id': '3.106',
        'parent': '2.9',
        'name': 'Paraguay',
        'value': 6811297
    }, {
        'id': '3.107',
        'parent': '2.9',
        'name': 'Uruguay',
        'value': 3456750
    }, {
        'id': '3.108',
        'parent': '2.9',
        'name': 'Guyana',
        'value': 777859
    }, {
        'id': '3.109',
        'parent': '2.9',
        'name': 'Suriname',
        'value': 563402
    }, {
        'id': '3.110',
        'parent': '2.9',
        'name': 'French Guiana',
        'value': 282731
    }, {
        'id': '3.111',
        'parent': '2.9',
        'name': 'Falkland Islands',
        'value': 2910
    },

    {
        'id': '2.8',
        'parent': '1.2',
        'name': 'Northern America'
    },

    {
        'id': '3.93',
        'parent': '2.8',
        'name': 'United States',
        'value': 324459463
    }, {
        'id': '3.94',
        'parent': '2.8',
        'name': 'Canada',
        'value': 36624199
    }, {
        'id': '3.95',
        'parent': '2.8',
        'name': 'Bermuda',
        'value': 61349
    }, {
        'id': '3.96',
        'parent': '2.8',
        'name': 'Greenland',
        'value': 56480
    }, {
        'id': '3.97',
        'parent': '2.8',
        'name': 'Saint Pierre and Miquelon',
        'value': 6320
    },

    {
        'id': '2.7',
        'parent': '1.2',
        'name': 'Central America'
    },

    {
        'id': '3.85',
        'parent': '2.7',
        'name': 'Mexico',
        'value': 129163276
    }, {
        'id': '3.86',
        'parent': '2.7',
        'name': 'Guatemala',
        'value': 16913503
    }, {
        'id': '3.87',
        'parent': '2.7',
        'name': 'Honduras',
        'value': 9265067
    }, {
        'id': '3.88',
        'parent': '2.7',
        'name': 'El Salvador',
        'value': 6377853
    }, {
        'id': '3.89',
        'parent': '2.7',
        'name': 'Nicaragua',
        'value': 6217581
    }, {
        'id': '3.90',
        'parent': '2.7',
        'name': 'Costa Rica',
        'value': 4905769
    }, {
        'id': '3.91',
        'parent': '2.7',
        'name': 'Panama',
        'value': 4098587
    }, {
        'id': '3.92',
        'parent': '2.7',
        'name': 'Belize',
        'value': 374681
    },

    {
        'id': '2.6',
        'parent': '1.2',
        'name': 'Caribbean'
    },

    {
        'id': '3.59',
        'parent': '2.6',
        'name': 'Cuba',
        'value': 11484636
    }, {
        'id': '3.60',
        'parent': '2.6',
        'name': 'Haiti',
        'value': 10981229
    }, {
        'id': '3.61',
        'parent': '2.6',
        'name': 'Dominican Republic',
        'value': 10766998
    }, {
        'id': '3.62',
        'parent': '2.6',
        'name': 'Puerto Rico',
        'value': 3663131
    }, {
        'id': '3.63',
        'parent': '2.6',
        'name': 'Jamaica',
        'value': 2890299
    }, {
        'id': '3.64',
        'parent': '2.6',
        'name': 'Trinidad and Tobago',
        'value': 1369125
    }, {
        'id': '3.65',
        'parent': '2.6',
        'name': 'Guadeloupe',
        'value': 449568
    }, {
        'id': '3.66',
        'parent': '2.6',
        'name': 'Bahamas',
        'value': 395361
    }, {
        'id': '3.67',
        'parent': '2.6',
        'name': 'Martinique',
        'value': 384896
    }, {
        'id': '3.68',
        'parent': '2.6',
        'name': 'Barbados',
        'value': 285719
    }, {
        'id': '3.69',
        'parent': '2.6',
        'name': 'Saint Lucia',
        'value': 178844
    }, {
        'id': '3.70',
        'parent': '2.6',
        'name': 'Curaçao',
        'value': 160539
    }, {
        'id': '3.71',
        'parent': '2.6',
        'name': 'Saint Vincent and the Grenadines',
        'value': 109897
    }, {
        'id': '3.72',
        'parent': '2.6',
        'name': 'Grenada',
        'value': 107825
    }, {
        'id': '3.73',
        'parent': '2.6',
        'name': 'Aruba',
        'value': 105264
    }, {
        'id': '3.74',
        'parent': '2.6',
        'name': 'United States Virgin Islands',
        'value': 104901
    }, {
        'id': '3.75',
        'parent': '2.6',
        'name': 'Antigua and Barbuda',
        'value': 102012
    }, {
        'id': '3.76',
        'parent': '2.6',
        'name': 'Dominica',
        'value': 73925
    }, {
        'id': '3.77',
        'parent': '2.6',
        'name': 'Cayman Islands',
        'value': 61559
    }, {
        'id': '3.78',
        'parent': '2.6',
        'name': 'Saint Kitts and Nevis',
        'value': 55345
    }, {
        'id': '3.79',
        'parent': '2.6',
        'name': 'Sint Maarten',
        'value': 40120
    }, {
        'id': '3.80',
        'parent': '2.6',
        'name': 'Turks and Caicos Islands',
        'value': 35446
    }, {
        'id': '3.81',
        'parent': '2.6',
        'name': 'British Virgin Islands',
        'value': 31196
    }, {
        'id': '3.82',
        'parent': '2.6',
        'name': 'Caribbean Netherlands',
        'value': 25398
    }, {
        'id': '3.83',
        'parent': '2.6',
        'name': 'Anguilla',
        'value': 14909
    }, {
        'id': '3.84',
        'parent': '2.6',
        'name': 'Montserrat',
        'value': 5177
    },
    /***********/

    /* Asia */
    {
        'id': '2.13',
        'parent': '1.3',
        'name': 'Southern Asia'
    },

    {
        'id': '3.136',
        'parent': '2.13',
        'name': 'India',
        'value': 1339180127
    }, {
        'id': '3.137',
        'parent': '2.13',
        'name': 'Pakistan',
        'value': 197015955
    }, {
        'id': '3.138',
        'parent': '2.13',
        'name': 'Bangladesh',
        'value': 164669751
    }, {
        'id': '3.139',
        'parent': '2.13',
        'name': 'Iran',
        'value': 81162788
    }, {
        'id': '3.140',
        'parent': '2.13',
        'name': 'Afghanistan',
        'value': 35530081
    }, {
        'id': '3.141',
        'parent': '2.13',
        'name': 'Nepal',
        'value': 29304998
    }, {
        'id': '3.142',
        'parent': '2.13',
        'name': 'Sri Lanka',
        'value': 20876917
    }, {
        'id': '3.143',
        'parent': '2.13',
        'name': 'Bhutan',
        'value': 807610
    }, {
        'id': '3.144',
        'parent': '2.13',
        'name': 'Maldives',
        'value': 436330
    },

    {
        'id': '2.11',
        'parent': '1.3',
        'name': 'Eastern Asia'
    },

    {
        'id': '3.117',
        'parent': '2.11',
        'name': 'China',
        'value': 1409517397
    }, {
        'id': '3.118',
        'parent': '2.11',
        'name': 'Japan',
        'value': 127484450
    }, {
        'id': '3.119',
        'parent': '2.11',
        'name': 'South Korea',
        'value': 50982212
    }, {
        'id': '3.120',
        'parent': '2.11',
        'name': 'North Korea',
        'value': 25490965
    }, {
        'id': '3.121',
        'parent': '2.11',
        'name': 'Taiwan',
        'value': 23626456
    }, {
        'id': '3.122',
        'parent': '2.11',
        'name': 'Hong Kong',
        'value': 7364883
    }, {
        'id': '3.123',
        'parent': '2.11',
        'name': 'Mongolia',
        'value': 3075647
    }, {
        'id': '3.124',
        'parent': '2.11',
        'name': 'Macau',
        'value': 622567
    },

    {
        'id': '2.12',
        'parent': '1.3',
        'name': 'South-Eastern Asia'
    },

    {
        'id': '3.125',
        'parent': '2.12',
        'name': 'Indonesia',
        'value': 263991379
    }, {
        'id': '3.126',
        'parent': '2.12',
        'name': 'Philippines',
        'value': 104918090
    }, {
        'id': '3.127',
        'parent': '2.12',
        'name': 'Vietnam',
        'value': 95540800
    }, {
        'id': '3.128',
        'parent': '2.12',
        'name': 'Thailand',
        'value': 69037513
    }, {
        'id': '3.129',
        'parent': '2.12',
        'name': 'Myanmar',
        'value': 53370609
    }, {
        'id': '3.130',
        'parent': '2.12',
        'name': 'Malaysia',
        'value': 31624264
    }, {
        'id': '3.131',
        'parent': '2.12',
        'name': 'Cambodia',
        'value': 16005373
    }, {
        'id': '3.132',
        'parent': '2.12',
        'name': 'Laos',
        'value': 6858160
    }, {
        'id': '3.133',
        'parent': '2.12',
        'name': 'Singapore',
        'value': 5708844
    }, {
        'id': '3.134',
        'parent': '2.12',
        'name': 'Timor-Leste',
        'value': 1296311
    }, {
        'id': '3.135',
        'parent': '2.12',
        'name': 'Brunei',
        'value': 428697
            // 'color': ''
    },

    {
        'id': '2.14',
        'parent': '1.3',
        'name': 'Western Asia'
    },

    {
        'id': '3.145',
        'parent': '2.14',
        'name': 'Turkey',
        'value': 80745020
    }, {
        'id': '3.146',
        'parent': '2.14',
        'name': 'Iraq',
        'value': 38274618
    }, {
        'id': '3.147',
        'parent': '2.14',
        'name': 'Saudi Arabia',
        'value': 32938213
    }, {
        'id': '3.148',
        'parent': '2.14',
        'name': 'Yemen',
        'value': 28250420
    }, {
        'id': '3.149',
        'parent': '2.14',
        'name': 'Syria',
        'value': 18269868
    }, {
        'id': '3.150',
        'parent': '2.14',
        'name': 'Azerbaijan',
        'value': 9827589
    }, {
        'id': '3.151',
        'parent': '2.14',
        'name': 'Jordan',
        'value': 9702353
    }, {
        'id': '3.152',
        'parent': '2.14',
        'name': 'United Arab Emirates',
        'value': 9400145
    }, {
        'id': '3.153',
        'parent': '2.14',
        'name': 'Israel',
        'value': 8321570
    }, {
        'id': '3.154',
        'parent': '2.14',
        'name': 'Lebanon',
        'value': 6082357
    }, {
        'id': '3.155',
        'parent': '2.14',
        'name': 'Palestine',
        'value': 4920724
    }, {
        'id': '3.156',
        'parent': '2.14',
        'name': 'Oman',
        'value': 4636262
    }, {
        'id': '3.157',
        'parent': '2.14',
        'name': 'Kuwait',
        'value': 4136528
    }, {
        'id': '3.158',
        'parent': '2.14',
        'name': 'Georgia',
        'value': 3912061
    }, {
        'id': '3.159',
        'parent': '2.14',
        'name': 'Armenia',
        'value': 2930450
    }, {
        'id': '3.160',
        'parent': '2.14',
        'name': 'Qatar',
        'value': 2639211
    }, {
        'id': '3.161',
        'parent': '2.14',
        'name': 'Bahrain',
        'value': 1492584
    },

    {
        'id': '2.10',
        'parent': '1.3',
        'name': 'Central Asia'
    },

    {
        'id': '3.112',
        'parent': '2.10',
        'name': 'Uzbekistan',
        'value': 31910641
    }, {
        'id': '3.113',
        'parent': '2.10',
        'name': 'Kazakhstan',
        'value': 18204499
    }, {
        'id': '3.114',
        'parent': '2.10',
        'name': 'Tajikistan',
        'value': 8921343
    }, {
        'id': '3.115',
        'parent': '2.10',
        'name': 'Kyrgyzstan',
        'value': 6045117
    }, {
        'id': '3.116',
        'parent': '2.10',
        'name': 'Turkmenistan',
        'value': 5758075
    },
    /***********/

    /* Europe */
    {
        'id': '2.15',
        'parent': '1.4',
        'name': 'Eastern Europe'
    },

    {
        'id': '3.162',
        'parent': '2.15',
        'name': 'Russia',
        'value': 143989754
    }, {
        'id': '3.163',
        'parent': '2.15',
        'name': 'Ukraine',
        'value': 44222947
    }, {
        'id': '3.164',
        'parent': '2.15',
        'name': 'Poland',
        'value': 38170712
    }, {
        'id': '3.165',
        'parent': '2.15',
        'name': 'Romania',
        'value': 19679306
    }, {
        'id': '3.166',
        'parent': '2.15',
        'name': 'Czechia',
        'value': 10618303
    }, {
        'id': '3.167',
        'parent': '2.15',
        'name': 'Hungary',
        'value': 9721559
    }, {
        'id': '3.168',
        'parent': '2.15',
        'name': 'Belarus',
        'value': 9468338
    }, {
        'id': '3.169',
        'parent': '2.15',
        'name': 'Bulgaria',
        'value': 7084571
    }, {
        'id': '3.170',
        'parent': '2.15',
        'name': 'Slovakia',
        'value': 5447662
    }, {
        'id': '3.171',
        'parent': '2.15',
        'name': 'Moldova',
        'value': 4051212
    }, {
        'id': '3.172',
        'parent': '2.15',
        'name': 'Cyprus',
        'value': 1179551
    },

    {
        'id': '2.16',
        'parent': '1.4',
        'name': 'Northern Europe'
    },

    {
        'id': '3.173',
        'parent': '2.16',
        'name': 'United Kingdom',
        'value': 66181585
    }, {
        'id': '3.174',
        'parent': '2.16',
        'name': 'Sweden',
        'value': 9910701
    }, {
        'id': '3.175',
        'parent': '2.16',
        'name': 'Denmark',
        'value': 5733551
    }, {
        'id': '3.176',
        'parent': '2.16',
        'name': 'Finland',
        'value': 5523231
    }, {
        'id': '3.177',
        'parent': '2.16',
        'name': 'Norway',
        'value': 5305383
    }, {
        'id': '3.178',
        'parent': '2.16',
        'name': 'Ireland',
        'value': 4761657
    }, {
        'id': '3.179',
        'parent': '2.16',
        'name': 'Lithuania',
        'value': 2890297
    }, {
        'id': '3.180',
        'parent': '2.16',
        'name': 'Latvia',
        'value': 1949670
    }, {
        'id': '3.181',
        'parent': '2.16',
        'name': 'Estonia',
        'value': 1309632
    }, {
        'id': '3.182',
        'parent': '2.16',
        'name': 'Iceland',
        'value': 335025
    }, {
        'id': '3.183',
        'parent': '2.16',
        'name': 'Guernsey and  Jersey',
        'value': 165314
    }, {
        'id': '3.184',
        'parent': '2.16',
        'name': 'Isle of Man',
        'value': 84287
    }, {
        'id': '3.185',
        'parent': '2.16',
        'name': 'Faroe Islands',
        'value': 49290
    },

    {
        'id': '2.17',
        'parent': '1.4',
        'name': 'Southern Europe'
    },

    {
        'id': '3.186',
        'parent': '2.17',
        'name': 'Italy',
        'value': 59359900
    }, {
        'id': '3.187',
        'parent': '2.17',
        'name': 'Spain',
        'value': 46354321
    }, {
        'id': '3.188',
        'parent': '2.17',
        'name': 'Greece',
        'value': 11159773
    }, {
        'id': '3.189',
        'parent': '2.17',
        'name': 'Portugal',
        'value': 10329506
    }, {
        'id': '3.190',
        'parent': '2.17',
        'name': 'Serbia',
        'value': 8790574
    }, {
        'id': '3.191',
        'parent': '2.17',
        'name': 'Croatia',
        'value': 4189353
    }, {
        'id': '3.192',
        'parent': '2.17',
        'name': 'Bosnia and Herzegovina',
        'value': 3507017
    }, {
        'id': '3.193',
        'parent': '2.17',
        'name': 'Albania',
        'value': 2930187
    }, {
        'id': '3.194',
        'parent': '2.17',
        'name': 'Republic of Macedonia',
        'value': 2083160
    }, {
        'id': '3.195',
        'parent': '2.17',
        'name': 'Slovenia',
        'value': 2079976
    }, {
        'id': '3.196',
        'parent': '2.17',
        'name': 'Montenegro',
        'value': 628960
    }, {
        'id': '3.197',
        'parent': '2.17',
        'name': 'Malta',
        'value': 430835
    }, {
        'id': '3.198',
        'parent': '2.17',
        'name': 'Andorra',
        'value': 76965
    }, {
        'id': '3.199',
        'parent': '2.17',
        'name': 'Gibraltar',
        'value': 34571
    }, {
        'id': '3.200',
        'parent': '2.17',
        'name': 'San Marino',
        'value': 33400
    }, {
        'id': '3.201',
        'parent': '2.17',
        'name': 'Vatican City',
        'value': 792
    },

    {
        'id': '2.18',
        'parent': '1.4',
        'name': 'Western Europe'
    },

    {
        'id': '3.202',
        'parent': '2.18',
        'name': 'Germany',
        'value': 82114224
    }, {
        'id': '3.203',
        'parent': '2.18',
        'name': 'France',
        'value': 64979548
    }, {
        'id': '3.204',
        'parent': '2.18',
        'name': 'Netherlands',
        'value': 17035938
    }, {
        'id': '3.205',
        'parent': '2.18',
        'name': 'Belgium',
        'value': 11429336
    }, {
        'id': '3.206',
        'parent': '2.18',
        'name': 'Austria',
        'value': 8735453
    }, {
        'id': '3.207',
        'parent': '2.18',
        'name': 'Switzerland',
        'value': 8476005
    }, {
        'id': '3.208',
        'parent': '2.18',
        'name': 'Luxembourg',
        'value': 583455
    }, {
        'id': '3.209',
        'parent': '2.18',
        'name': 'Monaco',
        'value': 38695
    }, {
        'id': '3.210',
        'parent': '2.18',
        'name': 'Liechtenstein',
        'value': 37922
    },
    /***********/

    /* Oceania */
    {
        'id': '2.19',
        'parent': '1.5',
        'name': 'Australia and New Zealand'
    },

    {
        'id': '3.211',
        'parent': '2.19',
        'name': 'Australia',
        'value': 24450561
    }, {
        'id': '3.212',
        'parent': '2.19',
        'name': 'New Zealand',
        'value': 4705818
    },

    {
        'id': '2.20',
        'parent': '1.5',
        'name': 'Melanesia'
    },

    {
        'id': '3.213',
        'parent': '2.20',
        'name': 'Papua New Guinea'
    }, {
        'id': '3.214',
        'parent': '2.20',
        'name': 'Fiji',
        'value': 905502
    }, {
        'id': '3.215',
        'parent': '2.20',
        'name': 'Solomon Islands',
        'value': 611343
    }, {
        'id': '3.216',
        'parent': '2.20',
        'name': 'New Caledonia',
        'value': 276255
    }, {
        'id': '3.217',
        'parent': '2.20',
        'name': 'Vanuatu',
        'value': 276244
    },

    {
        'id': '2.21',
        'parent': '1.5',
        'name': 'Micronesia'
    },

    {
        'id': '3.218',
        'parent': '2.21',
        'name': 'Guam',
        'value': 164229
    }, {
        'id': '3.219',
        'parent': '2.21',
        'name': 'Kiribati',
        'value': 116398
    }, {
        'id': '3.220',
        'parent': '2.21',
        'name': 'Federated States of Micronesia',
        'value': 105544
    }, {
        'id': '3.221',
        'parent': '2.21',
        'name': 'Northern Mariana Islands',
        'value': 55144
    }, {
        'id': '3.222',
        'parent': '2.21',
        'name': 'Marshall Islands',
        'value': 53127
    }, {
        'id': '3.223',
        'parent': '2.21',
        'name': 'Palau',
        'value': 21729
    }, {
        'id': '3.224',
        'parent': '2.21',
        'name': 'Nauru',
        'value': 11359
    },

    {
        'id': '2.22',
        'parent': '1.5',
        'name': 'Polynesia'
    },

    {
        'id': '3.225',
        'parent': '2.22',
        'name': 'French Polynesia',
        'value': 283007
    }, {
        'id': '3.226',
        'parent': '2.22',
        'name': 'Samoa',
        'value': 196440
    }, {
        'id': '3.227',
        'parent': '2.22',
        'name': 'Tonga',
        'value': 108020
    }, {
        'id': '3.228',
        'parent': '2.22',
        'name': 'American Samoa',
        'value': 55641
    }, {
        'id': '3.229',
        'parent': '2.22',
        'name': 'Cook Islands',
        'value': 17380
    }, {
        'id': '3.230',
        'parent': '2.22',
        'name': 'Wallis and Futuna',
        'value': 11773
    }, {
        'id': '3.231',
        'parent': '2.22',
        'name': 'Tuvalu',
        'value': 11192
    }, {
        'id': '3.232',
        'parent': '2.22',
        'name': 'Niue',
        'value': 1618
    }, {
        'id': '3.233',
        'parent': '2.22',
        'name': 'Tokelau',
        'value': 1300
    }
];

// variable
var barGraph;
var Cheight, Cwidth;

setTimeout(function() {

    if ($('#funnel-graph-ex').length > 0) {
        Highcharts.chart('funnel-graph-ex', {
            chart: {
                type: 'funnel'
            },
            title: {
                text: ''
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        useHTML: true,
                        enabled: true,
                        format: '<div style="text-align:center;">({point.y:,.0f})</div>',
                        // color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: false,
                        x: -120,
                        y: -50,
                        connectorWidth: 0
                    },
                    center: ['95%', '40%'],
                    neckWidth: '30%',
                    neckHeight: '0',
                    width: '80%',
                    //borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: false,
                // backgroundColor: "#fff",
                // borderWidth: 0,
                // shape: "none",
                // style: {
                //     fontSize: "13px",
                //     color: "#000",
                //     padding: 15,
                // },
                // padding: 15,
            },
            series: [{
                name: 'Unique users',
                data: [{
                        name: 'Total Audience',
                        y: 500000,
                        color: '#e8eeea'
                    },
                    // {
                    //     name: 'Rule 1',
                    //     y: 400000,
                    //     color: '#d3ddd5'
                    // },
                    // {
                    //     name: 'Rule 2',
                    //     y: 300000,
                    //     color: '#bdcdc3'
                    // }
                ]
            }]
        });
    }
    if ($('#funnel-graph-ex1').length > 0) {
        Highcharts.chart('funnel-graph-ex1', {
            chart: {
                type: 'funnel'
            },
            title: {
                text: ''
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        useHTML: true,
                        enabled: true,
                        format: '<div style="text-align:center;font-weight:300;color:#525252">{point.y:,.0f}</div>',
                        // color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: false,
                        // verticalAlign: 'top',
                        // layout: 'horizontal',
                        x: -120,
                        y: 0,
                        connectorWidth: 0,
                        fontSize: '10px',
                        fontWeight: 'regular',
                    },
                    markers: {
                        states: {
                            hover: {
                                enabled: false,
                                fillOpacity: 1
                            }
                        }

                    },
                    center: ['95%', '40%'],
                    neckWidth: '30%',
                    neckHeight: '0',
                    width: '80%',
                    //borderWidth: 0

                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: false,
                // backgroundColor: "#fff",
                // borderWidth: 0,
                // shape: "none",
                // style: {
                //     fontSize: "13px",
                //     color: "#000",
                //     padding: 15,
                // },
                // padding: 15,
            },
            series: [{
                name: 'Unique users',
                data: [{
                        name: 'Total Audience',
                        y: 500000,
                        color: '#e8eeea'
                    },
                    {
                        name: 'Rule 1',
                        y: 400000,
                        color: '#d3ddd5'
                    },
                    {
                        name: 'Rule 2',
                        y: 00000,
                        color: '#bdcdc3'
                    }
                    // , {
                    //     name: 'Rule 3',
                    //     y: 300000,
                    //     color: '#bdcdc3'
                    // }
                ]
            }]
        });
    }
    if ($('#funnel-graph-ex2').length > 0) {
        Highcharts.chart('funnel-graph-ex2', {
            chart: {
                type: 'funnel'
            },
            title: {
                text: ''
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        useHTML: true,
                        enabled: true,
                        align: "left",
                        format: '<div style="text-align:center;font-weight:300;color:#525252"><b>{point.name}</b> </br> ({point.y})</div>',
                        // color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: false,
                        x: 0,
                        y: '{ point.y }',
                        connectorWidth: 0
                    },
                    center: ['50%', '50%'],
                    neckWidth: '30%',
                    neckHeight: '10%',
                    width: '70%',
                    //borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: false,
                // backgroundColor: "#fff",
                // borderWidth: 0,
                // shape: "none",
                // style: {
                //     fontSize: "13px",
                //     color: "#000",
                //     padding: 15,
                // },
                // padding: 15,
            },
            series: [{
                name: 'Unique users',
                data: [{
                        name: 'Total Audience',
                        y: 7000,
                        color: '#e8eeea'
                    },
                    {
                        name: 'Rule 1',
                        y: 6000,
                        color: '#d3ddd5'
                    },
                    {
                        name: 'Rule 2',
                        y: 5000,
                        color: '#bdcdc3'
                    },
                    {
                        name: 'Rule 3',
                        y: 3000,
                        color: '#bdcdc3'
                    },
                    {
                        name: 'Rule 4',
                        y: 2000,
                        color: '#bdcdc3'
                    },
                    {
                        name: 'Rule 5',
                        y: 1400,
                        color: '#bdcdc3'
                    }
                ]
            }]
        });
    }
    if ($('#funnel-graph-ex3').length > 0) {
        Highcharts.chart('funnel-graph-ex3', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 680
                        }
                    }
                }]
            },
            chart: {
                type: 'funnel',
                showAxes: true,
                events: {
                    load: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    },
                    redraw: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    }
                }
            },
            yAxis: [{
                title: {
                    text: ''
                },
                labels: {
                    enabled: false,
                },
                min: 0,
                max: 500,
                gridLineWidth: 0,
                reversed: false,
                plotLines: [{
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 100, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 200, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 300, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 400, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 500, // Value of where the line will appear
                    width: 1 // Width of the line    
                }]
            }],
            title: {
                text: ''
            },


            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        inside: true,
                        crop: true,
                        connectorWidth: 0,
                        distance: 0,
                        useHTML: true,
                        style: {
                            textOutline: false,
                            fontSize: "12px",
                            color: "#666",
                            fontWeight: 'light',
                        },
                        formatter: function() {
                            console.log(this)
                            console.log('tt', this.series.options.data);

                            var letSeriesData = this.series.options.data
                            var actVal = letSeriesData[this.point.index];
                            // return '<b>' + actVal[0] + '</b> <br>' + actVal[2];
                            return '<b>' + actVal.name + '</b> <br>' + actVal.val;
                        },
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['50%', '50%'],
                    neckWidth: '0%',
                    neckHeight: '0%',
                    width: '50%'
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Unique users',
                data: [
                    { name: 'Website visits', color: '#e8eeea', y: 100, val: '50000' },
                    { name: 'Downloads', color: '#d3ddd5', y: 100, val: '40000' },
                    { name: 'Test label', color: '#bdcdc3', y: 100, val: '30000' },
                    { name: 'Test label 1', color: '#a6bbaf', y: 100, val: '20000' },
                    { name: 'Test label 2', color: '#84948b', y: 100, val: '10000' }
                ]
            }]
        });
    }
    if ($('#funnel-graph-ex4').length > 0) {
        Highcharts.chart('funnel-graph-ex4', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 760
                        }
                    }
                }]
            },
            chart: {
                type: 'funnel',
                showAxes: true,
                events: {
                    load: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    },
                    redraw: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    }
                }
            },
            yAxis: [{
                title: {
                    text: ''
                },
                labels: {
                    enabled: false,
                },
                min: 0,
                max: 500,
                gridLineWidth: 0,
                reversed: false,
                plotLines: [{
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 100, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 200, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 300, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 400, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 500, // Value of where the line will appear
                    width: 1 // Width of the line    
                }]
            }],
            title: {
                text: ''
            },


            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        inside: true,
                        crop: false,
                        connectorWidth: 0,
                        distance: 0,
                        useHTML: true,
                        style: {
                            textOutline: false,
                            fontSize: "12px",
                            color: "#666",
                            fontWeight: 'light',
                        },
                        formatter: function() {
                            console.log(this)
                            console.log('tt', this.series.options.data);

                            var letSeriesData = this.series.options.data
                            var actVal = letSeriesData[this.point.index];
                            return '<b>' + actVal.name + '</b> <br>' + actVal.val;
                        },
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['50%', '50%'],
                    neckWidth: '0%',
                    neckHeight: '0%',
                    width: '50%'
                },
                funnel: {
                    color: '#999'
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Unique users',
                data: [
                    { name: '', color: '#e8eeea', y: 100, val: '15654' },
                    { name: '', color: '#d3ddd5', y: 100, val: '4064' },
                    { name: '', color: '#bdcdc3', y: 100, val: '15654' },
                    { name: '', color: '#a6bbaf', y: 100, val: '3000' },
                    { name: '', color: '#84948b', y: 100, val: '100' }
                ]
            }]
        });
    }
    if ($('#funnel-graph-ex5').length > 0) {
        Highcharts.chart('funnel-graph-ex5', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 760
                        }
                    }
                }]
            },
            chart: {
                type: 'funnel',
                showAxes: true,
                events: {
                    load: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    },
                    redraw: function() {
                        console.log(this.series[0].userOptions);
                        var chart = this;
                        var tt = this.series[0].userOptions.data;
                        this.series[0].data.forEach(function(srs, i) {
                            //console.log(tt)
                            //srs.options.val = tt[i][2];
                            srs.dataLabel.attr({
                                x: chart.plotLeft,
                                y: srs.plotY - chart.series[0].data[0].plotY - 18,
                            })
                            console.log("srs", srs);
                        })
                    }
                }
            },
            yAxis: [{
                title: {
                    text: ''
                },
                labels: {
                    enabled: false,
                },
                min: 0,
                max: 500,
                gridLineWidth: 0,
                reversed: false,
                plotLines: [{
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 100, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 200, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 300, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 400, // Value of where the line will appear
                    width: 1 // Width of the line    
                }, {
                    color: '#c9c9c9', // Color value
                    // Style of the plot line. Default to solid
                    value: 500, // Value of where the line will appear
                    width: 1 // Width of the line    
                }]
            }],
            title: {
                text: ''
            },


            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        inside: true,
                        crop: false,
                        connectorWidth: 0,
                        distance: 0,
                        useHTML: true,
                        style: {
                            textOutline: false,
                            fontSize: "12px",
                            color: "#666",
                            fontWeight: 'light',
                        },
                        formatter: function() {
                            console.log(this)
                            console.log('tt', this.series.options.data);

                            var letSeriesData = this.series.options.data
                            var actVal = letSeriesData[this.point.index];
                            return '<b>' + actVal.name + '</b> <br>' + actVal.val;
                        },
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['50%', '50%'],
                    neckWidth: '0%',
                    neckHeight: '0%',
                    width: '50%'
                },
                funnel: {
                    color: '#999'
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Unique users',
                data: [
                    { name: '', color: '#e8eeea', y: 100, val: '15654' },
                    { name: '', color: '#d3ddd5', y: 100, val: '4064' },
                    { name: '', color: '#bdcdc3', y: 100, val: '15654' },
                    { name: '', color: '#a6bbaf', y: 100, val: '3000' },
                    { name: '', color: '#84948b', y: 100, val: '100' }
                ]
            }]
        });
    }
    if ($('#map-chart').length > 0) {
        // console.log(876);
        $.getJSON('https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/us-population-density.json', function(mapData) {
            console.log("data", mapData);
            // Make codes uppercase to match the map data
            $.each(mapData, function() {
                this.code = this.code.toUpperCase();
            });

            // Instantiate the map
            Highcharts.mapChart('map-chart', {
                responsive: responsiveData,
                chart: {
                    map: 'countries/us/us-all',
                    borderWidth: 1
                },

                title: {
                    text: ''
                },

                exporting: {
                    sourceWidth: 600,
                    sourceHeight: 500
                },

                legend: {
                    // layout: 'horizontal',
                    // borderWidth: 0,
                    // backgroundColor: 'rgba(255,255,255,0.85)',
                    // floating: true,
                    // verticalAlign: 'top',
                    // y: 25
                    enabled: false
                },

                mapNavigation: {
                    enabled: true
                },

                colorAxis: {
                    min: 1,
                    type: 'logarithmic',
                    minColor: '#cccccc',
                    maxColor: '#ff9c00',
                    stops: [
                        [0, '#ff9c00'],
                        [0.67, '#f0f2f4'],
                        [1, '#cccccc']
                    ]
                },

                series: [{
                    animation: {
                        duration: 1000
                    },
                    data: mapData,
                    joinBy: ['postal-code', 'code'],
                    dataLabels: {
                        enabled: true,
                        color: '#000',
                        format: '{point.code}',
                        style: {
                            textOutline: false,
                            fontSize: "10px",
                            color: "#000",
                            fontWeight: 'light',
                            padding: 15,
                        }
                    },
                    name: 'Population density',
                    tooltip: {
                        // pointFormat: '{point.code}: {point.value}/km²'
                    }
                }]
            });
        });
    }
    if ($('#stacked-bar').length > 0) {
        Highcharts.chart('stacked-bar', {
            responsive: responsiveData,
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes'],
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                },
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
            },
            legend: legends,
            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 18,
                    //pointPadding: 0,
                    //groupPadding:0.0
                }
            },
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7],
                color: Highcharts.getOptions().colors[15],
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2],
                color: Highcharts.getOptions().colors[4],
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2],
                color: Highcharts.getOptions().colors[11],
            }]
        });
    }
    if ($('#stacked-column1').length > 0) {
        Highcharts.chart('stacked-column1', {
            chart: {
                type: 'column',
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: ['01', '02', '03', '04', '05', '06', '07'],
                minPadding: 50,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Users  ( in Millions )',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            legend: legends,
            tooltip: {
                headerFormat: '{point.x}<br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#000",
                    padding: 15,
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    borderWidth: 0
                },
                series: {
                    borderWidth: 0,
                    pointWidth: 14,
                    //colors:Highcharts.getOptions().colors.reverse(),
                }
            },
            series: [{
                    name: 'John',
                    //color: '#a3ce76',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                }, {
                    name: 'Joe',
                    //color: '#cf6d84',
                    data: [5, 3, 4, 7, 2, 1, 2]
                }, {
                    name: 'Joe',
                    //color: '#e5d076',
                    data: [5, 3, 4, 7, 2, 2, 3]
                }, {
                    name: 'Joe',
                    //color: '#a4ce76',
                    data: [5, 3, 4, 7, 2, 5, 1]
                }, {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                }, {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                },
                {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                },
                {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a3ce76',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }
            ]
        });
    };
    if ($('#stacked-column').length > 0) {
        Highcharts.chart('stacked-column', {
            chart: {
                type: 'column',
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: ['01', '02', '03', '04', '05', '06', '07'],
                minPadding: 50,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Users  ( in Millions )',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            legend: legends,
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    borderWidth: 0,
                },
                series: {
                    borderWidth: 0,
                    pointWidth: 14,

                }
            },
            series: [{
                    name: 'Joe',
                    color: Highcharts.getOptions().colors[8],
                    data: [0.2, 0.2, 0.2, 0.5, 0.65, 0.5, 0.25]
                },
                {
                    name: 'John',
                    color: Highcharts.getOptions().colors[9],
                    data: [0.2, 0.15, 0.4, 0.2, 0.15, 0.25, 0.2]
                }, {
                    name: 'Jane',
                    color: Highcharts.getOptions().colors[7],
                    data: [0.3, 0.2, 0.2, 0.1, 0.1, 0.02, 0.25]
                }, {
                    name: 'Joe',
                    color: Highcharts.getOptions().colors[11],
                    data: [0.25, 0.1, 0.5, 0.15, 0.2, 0.2, 0.2]
                } //,
                // {
                //     name: 'John',
                //     color: Highcharts.getOptions().colors[1],
                //     data: [0.25, 0.25, 0.05, 0.1, 0.2, 0.2, 0.2]
                // }, {
                //     name: 'Jane',
                //     color: Highcharts.getOptions().colors[0],
                //     data: [0.15, 0.40, 0.40, 0.50, 0.25, 0.25, 0.6]
                // }
            ]
        });
    };
    if ($('#bar-graph').length > 0) {
        barGraph = Highcharts.chart('bar-graph', {
            chart: {
                type: 'column'
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            legend: legends,
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7'
                ],
                crosshair: true,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Rainfall (mm)',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px;">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 18,
                    borderRadius: 5,
                }
            },
            series: [{
                name: 'Tokyo',
                //color: Highcharts.getOptions().colors[12], //'#a4ce76',
                data: [1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25]

            }]
        });
        $(window).resize(function() {
            setTimeout(function() {
                Cheight = barGraph.height;
                Cwidth = $('#bar-graph').parents(".graph-wrap").width();
                //console.log(Cwidth);
                barGraph.setSize(Cwidth, Cheight, doAnimation = true);
            }, 1000);
        });
    };
    if ($('#line-graph').length > 0) {
        var lineGraph = Highcharts.chart('line-graph', {
            responsive: responsiveData,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                },
            },
            series: [{
                name: 'Installation',
                color: Highcharts.getOptions().colors[14],
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
        });
        $(window).resize(function() {
            setTimeout(function() {
                Cheight = lineGraph.height;
                Cwidth = $('#bar-graph').parents(".graph-wrap").width();
                //console.log(Cwidth);
                lineGraph.setSize(Cwidth, Cheight, doAnimation = true);
            }, 1000);
        });
    };
    if ($('#line-graph22').length > 0) {
        var lineGraph = Highcharts.chart('line-graph22', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 800
                    },
                    chartOptions: {
                        chart: {
                            height: 170
                        }
                    }
                }]
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                title: {
                    text: '',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                },
            },
            series: [{
                name: 'Installation',
                color: Highcharts.getOptions().colors[14],
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }],
            legend: false,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
        });
        $(window).resize(function() {
            setTimeout(function() {
                Cheight = lineGraph.height;
                Cwidth = $('#bar-graph').parents(".graph-wrap").width();
                //console.log(Cwidth);
                lineGraph.setSize(Cwidth, Cheight, doAnimation = true);
            }, 1000);
        });
    };
    if ($('#line-graph2').length > 0) {
        var lineGraph2 = Highcharts.chart('line-graph2', {
            responsive: responsiveData,
            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },
            series: [{
                name: 'Installation',
                //color: Highcharts.getOptions().colors[0],
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }, {
                name: 'Manufacturing',
                //color: '#cf6d84',
                //color: Highcharts.getOptions().colors[1],
                data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }

            // responsive: {
            //     rules: [{
            //         condition: {
            //             maxWidth: 500
            //         },
            //         chartOptions: {
            //             chart: {
            //                 height: 200
            //             },
            //             legend: {
            //                 layout: 'horizontal',
            //                 align: 'center',
            //                 verticalAlign: 'bottom'
            //             }
            //         }
            //     }]
            // }

        });
        $(window).resize(function() {
            setTimeout(function() {
                Cheight = lineGraph2.height;
                Cwidth = $('#bar-graph').parents(".graph-wrap").width();
                //console.log(Cwidth);
                lineGraph2.setSize(Cwidth, Cheight, doAnimation = true);
            }, 1000);
        });
    };
    if ($('#frequency-curve').length > 0) {
        Highcharts.chart('frequency-curve', {
            responsive: responsiveData,
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)',
                    style: {
                        color: "#000000"
                    }
                },
                min: 0,
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Winter 2012-2013',
                color: Highcharts.getOptions().colors[15],
                //color: '#a4ce76',
                // Define the data points. All series have a dummy year
                // of 1970/71 in order to be compared on the same x axis. Note
                // that in JavaScript, months start at 0 for January, 1 for February etc.
                data: [
                    [Date.UTC(1970, 9, 29), 0.5],
                    [Date.UTC(1970, 10, 9), 1.5],
                    [Date.UTC(1970, 11, 1), 1.0],
                    [Date.UTC(1971, 0, 1), 0.8],
                    [Date.UTC(1971, 0, 16), 0.7],
                    [Date.UTC(1971, 1, 19), 0.75],
                    [Date.UTC(1971, 2, 25), 0.4]
                ]
            }],
            legend: legends
        });
    }
    if ($('#frequency-polygon').length > 0) {
        Highcharts.chart('frequency-polygon', {
            responsive: responsiveData,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                },
                lineWidth: 1,
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },
            series: [{
                name: 'Installation',
                //color: '#e5d076',
                color: Highcharts.getOptions().colors[5],
                data: [43934, 57177, 43934, 69658, 43934, 119931, 53934, 154175]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
            // responsive: {
            //     rules: [{
            //         condition: {
            //             maxWidth: 500
            //         },
            //         chartOptions: {
            //             legend: {
            //                 layout: 'horizontal',
            //                 align: 'center',
            //                 verticalAlign: 'bottom'
            //             }
            //         }
            //     }]
            // }

        });
    }
    if ($('#pie-chart').length > 0) {
        Highcharts.chart('pie-chart', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            color: '#000000',
                            fontWeight: "bold",
                            borderColor: "transparent"
                        }
                    },
                    showInLegend: true,
                    size: '90%'
                }
            },
            legend: {
                align: "left",
                padding: 10,
                marginTop: 10,
                x: -10,
                y: 15,
                verticalAlign: 'bottom',
                maxHeight: 42,
                itemStyle: {
                    color: '#000',
                    fontWeight: 'light',
                    fontSize: '10px',
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[15],
                    y: 45
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[16],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: '(479,219) ',
                    //color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[14],
                    y: 5
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }]
            }],

        });
    }
    if ($('#pie-chartaa').length > 0) {
        Highcharts.chart('pie-chartaa', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            color: '#000000',
                            fontWeight: "bold",
                            borderColor: "transparent"
                        }
                    },
                    showInLegend: true,
                    size: '90%',
                }
            },
            legend: {
                align: "left",
                padding: 10,
                marginTop: 10,
                x: -5,
                y: 10,
                squareSymbol: false,
                itemMarginBottom: 18,
                itemDistance: 0,
                verticalAlign: 'bottom',
                symbolHeight: 4,
                symbolRadius: 2,
                symbolWidth: 15,
                useHTML: true,
                itemStyle: {
                    color: '#000',
                    fontWeight: 'light',
                    fontSize: '10px',
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[15],
                    y: 45
                }, {
                    name: '(479) ',
                    // color: Highcharts.getOptions().colors[16],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: '(479,5646) ',
                    //color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: '(47) ',
                    // color: Highcharts.getOptions().colors[14],
                    y: 5
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(47) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,2197857) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }]
            }],

        });
    }
    if ($('#pie-chartbb').length > 0) {
        Highcharts.chart('pie-chartbb', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            color: '#000000',
                            fontWeight: "bold",
                            borderColor: "transparent"
                        }
                    },
                    showInLegend: true,
                    size: '90%'
                }
            },
            legend: {
                enabled: false,
                align: "left",
                padding: 10,
                marginTop: 10,
                x: -10,
                y: 15,
                verticalAlign: 'bottom',
                maxHeight: 42,
                itemStyle: {
                    color: '#000',
                    fontWeight: 'light',
                    fontSize: '10px',
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[15],
                    y: 45
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[16],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: '(479,219) ',
                    //color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[14],
                    y: 5
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }]
            }],

        });
    }
    if ($('#pie-chartcc').length > 0) {
        Highcharts.chart('pie-chartcc', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            color: '#000000',
                            fontWeight: "bold",
                            borderColor: "transparent"
                        }
                    },
                    showInLegend: true,
                    size: '90%'
                }
            },
            legend: {
                enabled: true,
                align: "left",
                padding: 10,
                marginTop: 10,
                x: -10,
                y: 15,
                verticalAlign: 'bottom',
                maxHeight: 42,
                itemStyle: {
                    color: '#000',
                    fontWeight: 'light',
                    fontSize: '10px',
                },
                navigation: {
                    enabled: false,
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[15],
                    y: 45
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[16],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: '(479,219) ',
                    //color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[14],
                    y: 5
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }, {
                    name: '(479,219) ',
                    // color: Highcharts.getOptions().colors[13],
                    y: 15
                }]
            }],

        });
    }
    if ($('#pie-chart2').length > 0) {
        Highcharts.chart('pie-chart2', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: -5,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'a',
                    //color: '#e4cf75',
                    y: 45
                }, {
                    name: 'b',
                    //color: '#a483a4',
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'c',
                    //color: '#5fb6b8',
                    y: 10
                }, {
                    name: 'd',
                    //color: '#cf6c83',
                    y: 5
                }, {
                    name: 'e',
                    //color: '#a3ce76',
                    y: 15
                }]
            }]
        });
    }
    if ($('#dashboard-pie-chart1').length > 0) {
        Highcharts.chart('dashboard-pie-chart1', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 100,
                            width: 120
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 100,
                            width: 120
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: -5,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['80%', '0%']
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'a',
                    //color: '#e4cf75',
                    y: 45
                }, {
                    name: 'b',
                    //color: '#a483a4',
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'c',
                    //color: '#5fb6b8',
                    y: 10
                }, {
                    name: 'd',
                    //color: '#cf6c83',
                    y: 5
                }, {
                    name: 'e',
                    //color: '#a3ce76',
                    y: 15
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chart1').length > 0) {
        Highcharts.chart('dashboard-donut-chart1', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 100,
                            width: 100
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 100,
                            width: 100
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '80%',
                data: [{
                    name: '70%',
                    //color: '#e4cf75',
                    y: 70
                }, {
                    name: '30%',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chartaa').length > 0) {
        Highcharts.chart('dashboard-donut-chartaa', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',

                }
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false,
                        distance: -60,
                        style: {
                            fontSize: '8px',
                            fontWeight: 'light',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '75%',
                data: [{
                    name: 'lorem',
                    color: Highcharts.getOptions().colors[16],
                    y: 40
                }, {
                    name: 'lorem 1',
                    color: Highcharts.getOptions().colors[15],
                    y: 20
                }, {
                    name: 'lorem 2',
                    color: Highcharts.getOptions().colors[14],
                    y: 20
                }, {
                    name: 'lorem 3',
                    color: Highcharts.getOptions().colors[13],
                    y: 20
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chartab').length > 0) {
        Highcharts.chart('dashboard-donut-chartab', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',

                }
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false,
                        distance: -60,
                        style: {
                            fontSize: '8px',
                            fontWeight: 'light',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '75%',
                data: [{
                    name: 'lorem',
                    color: Highcharts.getOptions().colors[16],
                    y: 40
                }, {
                    name: 'lorem 1',
                    color: Highcharts.getOptions().colors[15],
                    y: 20
                }, {
                    name: 'lorem 2',
                    color: Highcharts.getOptions().colors[14],
                    y: 20
                }, {
                    name: 'lorem 3',
                    color: Highcharts.getOptions().colors[13],
                    y: 20
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chartac').length > 0) {
        Highcharts.chart('dashboard-donut-chartac', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',

                }
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false,
                        distance: -60,
                        style: {
                            fontSize: '8px',
                            fontWeight: 'light',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '75%',
                data: [{
                    name: 'lorem',
                    color: Highcharts.getOptions().colors[16],
                    y: 40
                }, {
                    name: 'lorem 1',
                    color: Highcharts.getOptions().colors[15],
                    y: 20
                }, {
                    name: 'lorem 2',
                    color: Highcharts.getOptions().colors[14],
                    y: 20
                }, {
                    name: 'lorem 3',
                    color: Highcharts.getOptions().colors[13],
                    y: 20
                }]
            }]
        });
    }
    if ($('#dashboard-pie-chart2').length > 0) {
        Highcharts.chart('dashboard-pie-chart2', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',

                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px'

                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'option1',
                    color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: 'option2',
                    color: Highcharts.getOptions().colors[16],
                    y: 20,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'option3',
                    color: Highcharts.getOptions().colors[15],
                    y: 40
                }, {
                    name: 'option4',
                    color: Highcharts.getOptions().colors[14],
                    y: 10
                }]
            }]
        });
    }
    if ($('#dashboard-pie-chart2a').length > 0) {
        Highcharts.chart('dashboard-pie-chart2a', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'option1',
                    color: Highcharts.getOptions().colors[16],
                    y: 30
                }, {
                    name: 'option2',
                    color: Highcharts.getOptions().colors[15],
                    y: 20,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'option3',
                    color: Highcharts.getOptions().colors[14],
                    y: 30
                }, {
                    name: 'option4',
                    color: Highcharts.getOptions().colors[13],
                    y: 20
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chart2').length > 0) {
        Highcharts.chart('dashboard-donut-chart2', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "10px",
                    color: "#666666"
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '80%',
                data: [{
                    name: '70%',
                    //color: '#e4cf75',
                    y: 70
                }, {
                    name: '30%',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#dashboard-pie-chart3').length > 0) {
        Highcharts.chart('dashboard-pie-chart3', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'a',
                    color: Highcharts.getOptions().colors[18],
                    y: 10
                }, {
                    name: 'b',
                    color: Highcharts.getOptions().colors[17],
                    y: 20,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'c',
                    color: Highcharts.getOptions().colors[16],
                    y: 40
                }, {
                    name: 'd',
                    color: Highcharts.getOptions().colors[15],
                    y: 10
                }, {
                    name: 'e',
                    color: Highcharts.getOptions().colors[14],
                    y: 20
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chart3').length > 0) {
        Highcharts.chart('dashboard-donut-chart3', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "10px",
                    color: "#666666"
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '80%',
                data: [{
                    name: '70%',
                    ///color: '#e4cf75',
                    y: 70
                }, {
                    name: '30%',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#dashboard-pie-chart4').length > 0) {
        Highcharts.chart('dashboard-pie-chart4', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: -5,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['60%', '7%']
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'a',
                    //color: '#e4cf75',
                    y: 45
                }, {
                    name: 'b',
                    //color: '#a483a4',
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'c',
                    //color: '#5fb6b8',
                    y: 10
                }, {
                    name: 'd',
                    //color: '#cf6c83',
                    y: 5
                }, {
                    name: 'e',
                    //color: '#a3ce76',
                    y: 15
                }]
            }]
        });
    }
    if ($('#dashboard-donut-chart4').length > 0) {
        Highcharts.chart('dashboard-donut-chart4', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 20
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "10px",
                    color: "#666666"
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        style: {

                            color: '#000000',
                            borderColor: "transparent",
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '80%',
                data: [{
                    name: '70%',
                    //color: '#e4cf75',
                    y: 70
                }, {
                    name: '30%',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#donut-chart1').length > 0) {
        Highcharts.chart('donut-chart1', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'DEC ‘16',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: false,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",
                        },
                    },
                    startAngle: -0,
                    endAngle: 360,
                    center: ['50%', '50%'],
                    showInLegend: true,
                    size: '90%'
                }
            },
            series: [{
                type: 'pie',
                name: 'DEC ‘16',
                innerSize: '80%',
                data: [{
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[11],
                    y: 70
                }, {
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[9],
                    y: 30
                }]
            }],
            legend: legends
        });
    }
    if ($('#donut-chart2').length > 0) {
        Highcharts.chart('donut-chart2', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: '',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -20,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",

                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: '',
                innerSize: '80%',
                data: [{
                    name: '70%',
                    //color: '#e4cf75',
                    y: 70
                }, {
                    name: '30%',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#donut-chart3').length > 0) {
        Highcharts.chart('donut-chart3', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'DEC ‘16',
                align: 'center',
                verticalAlign: 'middle',
                y: 0
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: '#000000',
                            borderColor: "transparent",

                        }
                    },
                    startAngle: -0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                type: 'pie',
                name: 'DEC ‘16',
                innerSize: '80%',
                data: [{
                    name: '(479,219) ',
                    //color: '#e4cf75',
                    y: 70
                }, {
                    name: '(479,219) ',
                    //color: '#a3ce76',
                    y: 30
                }]
            }]
        });
    }
    if ($('#scatter-plot').length > 0) {
        Highcharts.chart('scatter-plot', {
            responsive: responsiveData,
            chart: {
                type: 'scatter',
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: 'Height (cm)',
                    style: {
                        color: "#000000"
                    }
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Weight (kg)',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                scatter: {
                    marker: {
                        radius: 30,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        },
                        symbol: 'circle'
                            //width:90
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: '{point.x} cm, {point.y} kg',
                        backgroundColor: "#fff",
                        borderWidth: 0,
                        shape: "none",
                        style: {
                            fontSize: "13px",
                            color: "#666666"
                        },
                        padding: 15,
                    }
                }
            },
            series: [{
                name: 'Female',
                color: '#1b4867',
                radius: 70,
                data: [
                    [124, 81.6],
                    // [167.5, 59.0],
                    // [159.5, 49.2],
                    // [157.0, 63.0],
                    // [155.8, 53.6],
                    // [170.0, 59.0],
                    // [159.1, 47.6],
                    // [166.0, 69.8],
                    // [176.2, 66.8],
                    // [160.2, 75.2],
                    // [132.5, 55.2],
                    // [170.9, 54.2],
                    // [142.9, 62.5],
                    // [153.4, 42.0],
                    // [160.0, 50.0],
                    // [147.2, 49.8],
                    // [168.2, 49.2],
                    // [175.0, 73.2],
                    // [157.0, 47.8],
                    // [167.6, 68.8],
                    // [159.5, 50.6],
                    // [155.0, 82.5],
                    // [166.8, 57.2]
                ]

            }, {
                name: 'Male',
                color: '#45a6d3',
                data: [
                    [125.0, 80.6],
                    // [175.3, 71.8],
                    // [123.5, 80.7],
                    // [145.5, 72.6],
                    // [127.2, 78.8],
                    // [111.5, 74.8],
                    // [124.0, 86.4],
                    // [164.5, 78.4],
                    // [175.0, 62.0],
                    // [124.0, 81.6],
                    // [140.0, 76.6],
                    // [177.8, 83.6],
                    // [132.0, 90.0],
                    // [176.0, 74.6],
                    // [174.0, 71.0],
                    // [184.0, 79.6],
                    // [112.7, 93.8],
                    // [171.5, 70.0],
                    // [143.0, 72.4],
                    // [176.0, 85.9],
                    // [176.0, 78.8],
                    // [130.5, 77.8],
                    // [172.7, 66.2],
                    // [136.0, 86.4]
                ]
            }, {
                name: 'Other',
                color: '#93cbe4',
                data: [
                    [124.0, 72.6],
                    // [175.3, 71.8],
                    // [193.5, 80.7],
                    // [186.5, 72.6],
                    // [187.2, 78.8],
                    // [181.5, 74.8],
                    // [184.0, 86.4],
                    // [176.0, 86.4],
                    // [173.5, 81.8],
                    // [178.0, 85.6],
                    // [180.3, 82.8],
                    // [180.3, 76.4],
                    // [164.5, 63.2],
                    // [173.0, 60.9],
                    // [183.5, 74.8],
                    // [125.5, 70.0],
                    // [188.0, 72.4],
                    // [189.2, 84.1]
                ]
            }, {
                name: 'Other',
                color: '#c5c7c6',
                data: [
                    [128.0, 65.6],
                    [126.3, 72.8],
                    [125.5, 68.7],
                    // [106.5, 72.6],
                    // [167.2, 78.8],
                    // [191.5, 74.8],
                    // [134.0, 86.4],
                    // [146.0, 86.4],
                    // [183.5, 81.8],
                    // [178.0, 85.6],
                    // [130.3, 82.8],
                    // [150.3, 76.4],
                    // [114.5, 63.2],
                    // [123.0, 60.9],
                    // [163.5, 74.8],
                    // [185.5, 70.0],
                    // [158.0, 72.4],
                    // [109.2, 84.1]
                ]
            }],
            legend: legends,
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15
            },
        });
    }
    if ($('#cumulative-frequency').length > 0) {
        Highcharts.chart('cumulative-frequency', {
            responsive: responsiveData,
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date',
                    style: {
                        color: "#000000"
                    }
                },
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)',
                    style: {
                        color: "#000000"
                    }
                },
                min: 0,
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },

            series: [{
                name: '',
                //color: '#a483a4',
                color: Highcharts.getOptions().colors[11],
                data: [
                    [Date.UTC(1970, 9, 29), 0],
                    [Date.UTC(1970, 10, 9), 0.4],
                    [Date.UTC(1970, 11, 1), 0.25],
                    [Date.UTC(1971, 0, 1), 1.66],
                    [Date.UTC(1971, 0, 10), 1.8],
                    [Date.UTC(1971, 1, 19), 1.76],
                    [Date.UTC(1971, 2, 25), 2.62],
                    [Date.UTC(1971, 3, 19), 2.41]
                ]
            }],
            legend: legends
        });
    }
    if ($('#cumulative-frequency1').length > 0) {
        Highcharts.chart('cumulative-frequency1', {
            responsive: responsiveData,
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date',
                    style: {
                        color: "#000000"
                    }
                },
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)',
                    style: {
                        color: "#000000"
                    }
                },
                min: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },

            plotOptions: {
                spline: {
                    marker: {
                        enabled: false
                    }
                }
            },

            series: [{
                name: '',
                //color: '#a483a4',
                data: [
                    [Date.UTC(1970, 9, 29), 0],
                    [Date.UTC(1970, 10, 9), 0.4],
                    [Date.UTC(1970, 11, 1), 0.25],
                    [Date.UTC(1971, 0, 1), 1.66],
                    [Date.UTC(1971, 0, 10), 1.8],
                    [Date.UTC(1971, 1, 19), 1.76],
                    [Date.UTC(1971, 2, 25), 2.62],
                    [Date.UTC(1971, 3, 19), 2.41]
                ]
            }]
        });
    }
    if ($('#area-chart').length > 0) {
        Highcharts.chart('area-chart', {
            responsive: responsiveData,
            chart: {
                type: 'area'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                allowDecimals: false,
                labels: {
                    formatter: function() {
                        return this.value; // clean, unformatted number for year
                    }
                },
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Nuclear weapon states',
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value / 1000 + 'k';
                    },

                    style: {
                        color: '#000000'
                    }

                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb"
            },
            tooltip: {
                pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666",
                },
                padding: 15,
            },
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: [{
                name: 'USA',
                color: Highcharts.getOptions().colors[0],
                data: [0, 6, 11, 32, 110, 235, 369, 640, 1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
                    27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
                    26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
                    24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586,
                    22380, 21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950,
                    10871, 10824, 10577, 10527, 10475, 10421, 10358, 10295, 10104, 10000, 1060, 566, 344, 211, 111, 90, 50, 22, 0
                ]
            }, {
                name: 'USSR',
                color: Highcharts.getOptions().colors[2],
                data: [0, 5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                    4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                    15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
                    33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                    35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                    21000, 20000, 19000, 18000, 18000, 17000, 16000, 10000, 1060, 566, 344, 211, 111, 90, 50, 22, 0
                ]
            }, {
                name: 'Russia',
                color: Highcharts.getOptions().colors[3],
                data: [null,
                    5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                    4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                    15915, 17385, 19055, 32049,
                    33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                    35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                    21000, 10000, 12000, 1300, 16000, 17000, 10000, 1060, 566, 344, 211, 111, 90, 50, 22, 0
                ]
            }],
            legend: legends
        });
    }
    if ($('#line-bar').length > 0) {
        Highcharts.chart('line-bar', {
            responsive: responsiveData,
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ],
                crosshair: true,
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}°C',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Rainfall',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} mm',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true,
                gridLineWidth: 0,
                visible: false
            }],
            tooltip: {
                shared: true,
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    borderColor: '#ff0000',
                    pointWidth: 16
                }
            },
            series: [{
                name: 'Rainfall',
                type: 'column',
                color: Highcharts.getOptions().colors[8],
                yAxis: 1,
                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6],
                tooltip: {
                    valueSuffix: ' mm'
                }

            }, {
                name: 'Temperature',
                type: 'spline',
                color: Highcharts.getOptions().colors[10],
                data: [20.8, 24.9, 29, 32, 36.2, 43.5, 35.2],
                tooltip: {
                    valueSuffix: '°C'
                }
            }],
            legend: legends
        });
    }
    if ($('#multi-bars1').length > 0) {
        Highcharts.chart('multi-bars1', {
            responsive: responsiveData,
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7'
                ],
                crosshair: true,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall (mm)',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666",
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'a',
                    color: Highcharts.getOptions().colors[3],
                    data: [40.4, 40.4, 34.5, 37.7, 52.6, 75.5, 55.4]

                }, {
                    name: 'b',
                    color: Highcharts.getOptions().colors[4],
                    data: [43.0, 43.2, 36.5, 39.7, 54.6, 78.5, 57.4]

                }, {
                    name: 'c',
                    color: Highcharts.getOptions().colors[0],
                    data: [45.9, 45.8, 39.3, 41.4, 57.0, 82.3, 59.0]

                }, {
                    name: 'd',
                    color: Highcharts.getOptions().colors[1],
                    data: [44.4, 44.2, 34.5, 39.7, 54.6, 82.5, 57.4]

                } //, {
                //     name: 'f',
                //     color: Highcharts.getOptions().colors[2],
                //     data: [42.4, 42.2, 36.5, 37.7, 52.6, 78.5, 55.4]

                // }, {
                //     name: 'g',
                //     color: Highcharts.getOptions().colors[5],
                //     data: [40.4, 40.2, 34.5, 36.7, 50.6, 75.5, 52.4]

                // }
            ],
            legend: legends
        });
    }
    if ($('#multi-bars2').length > 0) {
        Highcharts.chart('multi-bars2', {
            responsive: responsiveData,
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7'
                ],
                crosshair: true,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rainfall (mm)',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'London',
                color: Highcharts.getOptions().colors[3],
                data: [48.9, 45.8, 24.3, 24.4, 50.0, 34.3, 20.0]

            }, {
                name: 'Berlin',
                color: Highcharts.getOptions().colors[4],
                data: [25.4, 39.2, 40.5, 39.7, 12.6, 44.5, 30.4]

            }],
            legend: legends
        });
    }
    if ($('#multi-bars3').length > 0) {
        Highcharts.chart('multi-bars3', {
            responsive: responsiveData,
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                offset: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                series: {
                    pointStart: Date.UTC(2017, 0, 29),
                    pointInterval: 36e5
                }
            },
            series: [{
                type: 'area',
                keys: ['y', 'rotation'], // rotation is not used here
                data: [
                    [9.8, 177.9],
                    [10.1, 177.2],
                    [11.3, 179.7],
                    [10.9, 175.5],
                    [9.3, 159.9],
                    [8.8, 159.6],
                    [7.8, 162.6],
                    [5.6, 186.2],
                    [6.8, 146.0],
                    [6.4, 139.9],
                    [3.1, 180.2],
                    [4.3, 177.6],
                    [5.3, 191.8],
                    [6.3, 173.1],
                    [7.7, 140.2],
                    [8.5, 136.1],
                    [9.4, 142.9],
                    [10.0, 140.4],
                    [5.3, 142.1],
                    [3.8, 141.0],
                    [3.3, 116.5],
                    [1.5, 327.5],
                    [0.1, 1.1],
                    [1.2, 11.1]
                ],
                //color: Highcharts.getOptions().colors[0],
                fillColor: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, Highcharts.getOptions().colors[1]],
                        [1, Highcharts.color(Highcharts.getOptions().colors[1])
                            .setOpacity(0.25).get()
                        ]
                    ]
                },
                name: 'Wind speed',
                tooltip: {
                    valueSuffix: ' m/s',
                    backgroundColor: "#fff",
                    borderWidth: 0,
                    shape: "none",
                    style: {
                        fontSize: "10px",
                        color: "#666666"
                    }
                }
            }],
            legend: legends,
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
        });
    }
    if ($('#multi-bars4').length > 0) {
        var multiBars4 = Highcharts.chart('multi-bars4', {
            responsive: responsiveData,
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: 'Temperature (°C)',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Tokyo',
                data: [8.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
                color: Highcharts.getOptions().colors[11],
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8],
                color: Highcharts.getOptions().colors[8],
            }],
            legend: legends,
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
        });

        $(window).resize(function() {
            setTimeout(function() {
                Cheight = multiBars4.height;
                Cwidth = $('#bar-graph').parents(".graph-wrap").width();
                //console.log(Cwidth);
                multiBars4.setSize(Cwidth, Cheight, doAnimation = true);
            }, 1000);
        });
    }
    if ($('#3D-pie').length > 0) {
        Highcharts.chart('3D-pie', {
            responsive: responsiveData,
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [{
                        name: 'Firefox',
                        y: 45.0,
                        color: Highcharts.getOptions().colors[1],
                    },
                    {
                        name: 'IE',
                        y: 26.8,
                        color: Highcharts.getOptions().colors[0],
                    },
                    {
                        name: 'Chrome',
                        y: 12.8,
                        sliced: true,
                        selected: true,
                        color: Highcharts.getOptions().colors[5],
                    },
                    {
                        name: 'Safari',
                        y: 8.5,
                        color: Highcharts.getOptions().colors[2],
                    },
                    {
                        name: 'Opera',
                        y: 6.2,
                        color: Highcharts.getOptions().colors[3],
                    },
                    {
                        name: 'Others',
                        y: 0.7,
                        color: Highcharts.getOptions().colors[4],
                    }
                ]
            }],
            legend: legends
        });
    }
    if ($('#funnel').length > 0) {
        Highcharts.chart('funnel', {
            responsive: responsiveData,
            chart: {
                type: 'funnel'
            },
            title: {
                text: ''
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name} ({point.y:,.0f})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['45%', '45%'],
                    neckWidth: '25%',
                    neckHeight: '40%',
                    width: '74%',
                }
            },
            series: [{
                name: 'Unique users',
                data: [
                    ['Website visits', 654],
                    ['Downloads', 1064],
                    ['Requested price list', 987],
                    ['Invoice sent', 976],
                    ['Finalized', 846]
                ]
            }],
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            legend: legends
        });
    }
    if ($('#bubble-chart').length > 0) {
        Highcharts.chart('bubble-chart', {
            chart: {
                type: 'bubble',
                // plotBorderWidth: 1,
                zoomType: 'xy'
            },
            responsive: responsiveData,
            legend: legends,

            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },

            xAxis: {
                gridLineWidth: 0,
                tickLength: 0,
                title: {
                    text: 'Phone'
                },
                labels: {
                    format: '{value} gr'
                },
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 0,
                    value: 65,
                    label: {
                        rotation: 0,
                        y: 15,
                        style: {
                            fontStyle: 'italic'
                        },
                        text: ''
                    },
                    zIndex: 3
                }]
            },

            yAxis: {
                startOnTick: false,
                endOnTick: false,
                lineColor: "#ebebeb",
                lineWidth: 1,
                title: {
                    text: 'Usage'
                },
                labels: {
                    format: '{value} gr'
                },
                maxPadding: 0.2,
                plotLines: [{
                    color: 'black',
                    dashStyle: 'dot',
                    width: 0,
                    value: 50,
                    label: {
                        align: 'right',
                        style: {
                            fontStyle: 'italic'
                        },
                        text: '',
                        x: -10
                    },
                    zIndex: 3
                }],
                gridLineWidth: 0
            },

            tooltip: {
                headerFormat: '{point.x}<br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#000",
                    padding: 15,
                },
                padding: 15,
            },
            // {
            //     useHTML: true,
            //     headerFormat: '<table>',
            //     pointFormat: '<tr><th colspan="2"><h3>{point.country}</h3></th></tr>' +
            //         '<tr><th>Fat intake:</th><td>{point.x}g</td></tr>' +
            //         '<tr><th>Sugar intake:</th><td>{point.y}g</td></tr>' +
            //         '<tr><th>Obesity (adults):</th><td>{point.z}%</td></tr>',
            //     footerFormat: '</table>',
            //     followPointer: true
            // },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        useHTML: true,
                        format: '<table><tr><td style="text-align:center">{point.name}</td></tr> <tr><td style="text-align:center">{point.z}</td></tr></table>',
                        style: {
                            fontWeight: 'normal',
                        }
                    }
                },
                bubble: {
                    maxSize: '55%',
                    minSize: '15%',
                    marker: {
                        lineWidth: 0,
                        fillOpacity: 1,
                    }
                }
            },

            series: [{
                data: [
                    // { x: 95, y: 95, z: 13.8, name: 'BE', country: 'Belgium' },
                    // { x: 86.5, y: 102.9, z: 14.7, name: 'DE', country: 'Germany' },
                    // { x: 80.8, y: 91.5, z: 15.8, name: 'FI', country: 'Finland' },
                    // { x: 80.4, y: 102.5, z: 12, name: 'NL', country: 'Netherlands' },
                    // { x: 80.3, y: 86.1, z: 11.8, name: 'SE', country: 'Sweden' },
                    // { x: 78.4, y: 70.1, z: 16.6, name: 'ES', country: 'Spain' },
                    // { x: 74.2, y: 68.5, z: 14.5, name: 'FR', country: 'France' },
                    // { x: 73.5, y: 83.1, z: 10, name: 'NO', country: 'Norway' },
                    // { x: 71, y: 93.2, z: 24.7, name: 'UK', country: 'United Kingdom' },
                    { x: 65.453, y: 90.6, z: 10.4, name: '', country: '', color: '#ccc' },
                    { x: 65.48, y: 20, z: 16, name: '', country: '', color: '#9ce6cf' },
                    { x: 65.442, y: 128.4, z: 35.3, name: 'Android', country: 'Android', color: '#ffdd65' },
                    { x: 65.432, y: 38, z: 25.5, name: 'iPhone', country: 'iPhone', color: '#ff9c9c' },
                    { x: 65.45, y: 36, z: 15.4, name: '', country: '', color: '#74d3ff' },
                    { x: 65.423, y: 145.9, z: 51, name: 'Flash', country: 'Flash', color: '#a290fc' }
                ]
            }]
        });
    }
    if ($('#sankey').length > 0) {
        Highcharts.chart('sankey', {
            responsive: responsiveData,
            title: {
                text: ''
            },

            series: [{
                keys: ['from', 'to', 'weight'],
                data: [
                    ['Brazil', 'Portugal', 5],
                    ['Brazil', 'France', 1],
                    ['Brazil', 'Spain', 1],
                    ['Brazil', 'England', 1],
                    ['Canada', 'Portugal', 1],
                    ['Canada', 'France', 5],
                    ['Canada', 'England', 1],
                    ['Mexico', 'Portugal', 1],
                    ['Mexico', 'France', 1],
                    ['Mexico', 'Spain', 5],
                    ['Mexico', 'England', 1],
                    ['USA', 'Portugal', 1],
                    ['USA', 'France', 1],
                    ['USA', 'Spain', 1],
                    ['USA', 'England', 5],
                    ['Portugal', 'Angola', 2],
                    ['Portugal', 'Senegal', 1],
                    ['Portugal', 'Morocco', 1],
                    ['Portugal', 'South Africa', 3],
                    ['France', 'Angola', 1],
                    ['France', 'Senegal', 3],
                    ['France', 'Mali', 3],
                    ['France', 'Morocco', 3],
                    ['France', 'South Africa', 1],
                    ['Spain', 'Senegal', 1],
                    ['Spain', 'Morocco', 3],
                    ['Spain', 'South Africa', 1],
                    ['England', 'Angola', 1],
                    ['England', 'Senegal', 1],
                    ['England', 'Morocco', 2],
                    ['England', 'South Africa', 7],
                    ['South Africa', 'China', 5],
                    ['South Africa', 'India', 1],
                    ['South Africa', 'Japan', 3],
                    ['Angola', 'China', 5],
                    ['Angola', 'India', 1],
                    ['Angola', 'Japan', 3],
                    ['Senegal', 'China', 5],
                    ['Senegal', 'India', 1],
                    ['Senegal', 'Japan', 3],
                    ['Mali', 'China', 5],
                    ['Mali', 'India', 1],
                    ['Mali', 'Japan', 3],
                    ['Morocco', 'China', 5],
                    ['Morocco', 'India', 1],
                    ['Morocco', 'Japan', 3]
                ],
                type: 'sankey',
                name: 'Sankey demo series',
                dataLabels: {
                    enabled: true,
                    color: '#000',
                    style: {
                        textOutline: false,
                        fontSize: "10px",
                        color: "#000",
                        fontWeight: 'light',
                        padding: 15,
                    }
                },
            }]

        });
    }
    if ($('#sunburst').length > 0) {
        // Splice in transparent for the center circle
        Highcharts.getOptions().colors.splice(0, 0, 'transparent');
        Highcharts.chart('sunburst', {
            responsive: responsiveData,
            chart: {
                height: '100%'
            },

            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            series: [{
                type: "sunburst",
                data: data,
                allowDrillToNode: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '{point.name}',
                    filter: {
                        property: 'innerArcLength',
                        operator: '>',
                        value: 16
                    }
                },
                levels: [{
                        level: 1,
                        levelIsConstant: false,
                        dataLabels: {
                            rotationMode: 'parallel',
                            filter: {
                                property: 'outerArcLength',
                                operator: '>',
                                value: 64
                            }
                        },
                        color: Highcharts.getOptions().colors[0]
                    }, {
                        level: 2,
                        colorByPoint: true,
                        dataLabels: {
                            rotationMode: 'parallel'
                        },
                        color: Highcharts.getOptions().colors[1]
                    },
                    {
                        level: 3,
                        colorVariation: {
                            key: 'brightness',
                            to: -0.5
                        },
                        color: Highcharts.getOptions().colors[2]
                    }, {
                        level: 4,
                        colorVariation: {
                            key: 'brightness',
                            to: 0.5
                        },
                        color: Highcharts.getOptions().colors[3]
                    }
                ]

            }],
            tooltip: {
                headerFormat: "",
                pointFormat: 'The population of <b>{point.name}</b> is <b>{point.value}</b>'
            }
        });
    }
    if ($('#tree').length > 0) {
        var data11 = {
                'South-East Asia': {
                    'Sri Lanka': {
                        'Communicable & other Group I': '75.5',
                        'Injuries': '89.0',
                        'Noncommunicable diseases': '501.2'
                    },
                    'Bangladesh': {
                        'Noncommunicable diseases': '548.9',
                        'Injuries': '64.0',
                        'Communicable & other Group I': '234.6'
                    },
                    'Myanmar': {
                        'Communicable & other Group I': '316.4',
                        'Injuries': '102.0',
                        'Noncommunicable diseases': '708.7'
                    },
                    'Maldives': {
                        'Injuries': '35.0',
                        'Noncommunicable diseases': '487.2',
                        'Communicable & other Group I': '59.2'
                    },
                    'Democratic People\'s Republic of Korea': {
                        'Injuries': '91.9',
                        'Noncommunicable diseases': '751.4',
                        'Communicable & other Group I': '117.3'
                    },
                    'Bhutan': {
                        'Injuries': '142.2',
                        'Noncommunicable diseases': '572.8',
                        'Communicable & other Group I': '186.9'
                    },
                    'Thailand': {
                        'Injuries': '72.8',
                        'Communicable & other Group I': '123.3',
                        'Noncommunicable diseases': '449.1'
                    },
                    'Nepal': {
                        'Noncommunicable diseases': '678.1',
                        'Injuries': '88.7',
                        'Communicable & other Group I': '251.8'
                    },
                    'Timor-Leste': {
                        'Injuries': '69.2',
                        'Noncommunicable diseases': '670.5',
                        'Communicable & other Group I': '343.5'
                    },
                    'India': {
                        'Communicable & other Group I': '253.0',
                        'Injuries': '115.9',
                        'Noncommunicable diseases': '682.3'
                    },
                    'Indonesia': {
                        'Injuries': '49.3',
                        'Noncommunicable diseases': '680.1',
                        'Communicable & other Group I': '162.4'
                    }
                },
                'Europe': {
                    'Hungary': {
                        'Communicable & other Group I': '16.8',
                        'Noncommunicable diseases': '602.8',
                        'Injuries': '44.3'
                    },
                    'Poland': {
                        'Communicable & other Group I': '22.6',
                        'Noncommunicable diseases': '494.5',
                        'Injuries': '48.9'
                    },
                    'Israel': {
                        'Communicable & other Group I': '31.2',
                        'Noncommunicable diseases': '311.2',
                        'Injuries': '20.8'
                    },
                    'France': {
                        'Communicable & other Group I': '21.4',
                        'Noncommunicable diseases': '313.2',
                        'Injuries': '34.6'
                    },
                    'Turkey': {
                        'Injuries': '39.1',
                        'Communicable & other Group I': '43.9',
                        'Noncommunicable diseases': '555.2'
                    },
                    'Kyrgyzstan': {
                        'Communicable & other Group I': '65.8',
                        'Injuries': '65.1',
                        'Noncommunicable diseases': '835.4'
                    },
                    'Croatia': {
                        'Communicable & other Group I': '12.2',
                        'Noncommunicable diseases': '495.8',
                        'Injuries': '40.1'
                    },
                    'Portugal': {
                        'Injuries': '25.2',
                        'Communicable & other Group I': '39.9',
                        'Noncommunicable diseases': '343.3'
                    },
                    'Greece': {
                        'Injuries': '26.5',
                        'Noncommunicable diseases': '365.0',
                        'Communicable & other Group I': '24.1'
                    },
                    'Italy': {
                        'Injuries': '20.1',
                        'Communicable & other Group I': '15.5',
                        'Noncommunicable diseases': '303.6'
                    },
                    'Belgium': {
                        'Communicable & other Group I': '27.8',
                        'Injuries': '38.9',
                        'Noncommunicable diseases': '356.8'
                    },
                    'Lithuania': {
                        'Noncommunicable diseases': '580.6',
                        'Communicable & other Group I': '25.5',
                        'Injuries': '76.4'
                    },
                    'Uzbekistan': {
                        'Communicable & other Group I': '85.8',
                        'Injuries': '47.4',
                        'Noncommunicable diseases': '810.9'
                    },
                    'Serbia': {
                        'Communicable & other Group I': '19.4',
                        'Injuries': '32.0',
                        'Noncommunicable diseases': '657.7'
                    },
                    'Austria': {
                        'Noncommunicable diseases': '359.5',
                        'Injuries': '30.6',
                        'Communicable & other Group I': '12.6'
                    },
                    'Bosnia and Herzegovina': {
                        'Injuries': '42.4',
                        'Noncommunicable diseases': '512.5',
                        'Communicable & other Group I': '20.0'
                    },
                    'Slovakia': {
                        'Injuries': '39.1',
                        'Communicable & other Group I': '35.3',
                        'Noncommunicable diseases': '532.5'
                    },
                    'The former Yugoslav republic of Macedonia': {
                        'Injuries': '24.0',
                        'Communicable & other Group I': '16.9',
                        'Noncommunicable diseases': '636.5'
                    },
                    'Sweden': {
                        'Communicable & other Group I': '19.3',
                        'Noncommunicable diseases': '333.5',
                        'Injuries': '26.1'
                    },
                    'Russian Federation': {
                        'Noncommunicable diseases': '790.3',
                        'Communicable & other Group I': '73.8',
                        'Injuries': '102.8'
                    },
                    'Republic of Moldova': {
                        'Noncommunicable diseases': '787.6',
                        'Injuries': '75.7',
                        'Communicable & other Group I': '44.5'
                    },
                    'Ireland': {
                        'Injuries': '31.8',
                        'Communicable & other Group I': '21.5',
                        'Noncommunicable diseases': '343.9'
                    },
                    'Estonia': {
                        'Injuries': '47.0',
                        'Communicable & other Group I': '18.5',
                        'Noncommunicable diseases': '510.7'
                    },
                    'Cyprus': {
                        'Noncommunicable diseases': '333.0',
                        'Injuries': '26.6',
                        'Communicable & other Group I': '16.2'
                    },
                    'Kazakhstan': {
                        'Noncommunicable diseases': '949.7',
                        'Injuries': '101.6',
                        'Communicable & other Group I': '55.3'
                    },
                    'Netherlands': {
                        'Noncommunicable diseases': '355.2',
                        'Injuries': '22.3',
                        'Communicable & other Group I': '25.5'
                    },
                    'Finland': {
                        'Noncommunicable diseases': '366.6',
                        'Injuries': '38.7',
                        'Communicable & other Group I': '9.0'
                    },
                    'Romania': {
                        'Noncommunicable diseases': '612.2',
                        'Injuries': '40.7',
                        'Communicable & other Group I': '38.5'
                    },
                    'Albania': {
                        'Noncommunicable diseases': '671.6',
                        'Injuries': '48.0',
                        'Communicable & other Group I': '46.5'
                    },
                    'Iceland': {
                        'Injuries': '29.0',
                        'Noncommunicable diseases': '311.7',
                        'Communicable & other Group I': '14.0'
                    },
                    'Azerbaijan': {
                        'Noncommunicable diseases': '664.3',
                        'Injuries': '33.6',
                        'Communicable & other Group I': '70.8'
                    },
                    'Tajikistan': {
                        'Injuries': '51.6',
                        'Communicable & other Group I': '147.7',
                        'Noncommunicable diseases': '752.6'
                    },
                    'Bulgaria': {
                        'Communicable & other Group I': '33.4',
                        'Injuries': '36.4',
                        'Noncommunicable diseases': '638.2'
                    },
                    'United Kingdom of Great Britain and Northern Ireland': {
                        'Communicable & other Group I': '28.5',
                        'Injuries': '21.5',
                        'Noncommunicable diseases': '358.8'
                    },
                    'Spain': {
                        'Communicable & other Group I': '19.1',
                        'Injuries': '17.8',
                        'Noncommunicable diseases': '323.1'
                    },
                    'Ukraine': {
                        'Communicable & other Group I': '69.3',
                        'Injuries': '67.3',
                        'Noncommunicable diseases': '749.0'
                    },
                    'Norway': {
                        'Noncommunicable diseases': '336.6',
                        'Communicable & other Group I': '25.2',
                        'Injuries': '25.6'
                    },
                    'Denmark': {
                        'Injuries': '22.5',
                        'Communicable & other Group I': '29.5',
                        'Noncommunicable diseases': '406.1'
                    },
                    'Belarus': {
                        'Noncommunicable diseases': '682.5',
                        'Communicable & other Group I': '28.3',
                        'Injuries': '91.3'
                    },
                    'Malta': {
                        'Noncommunicable diseases': '364.5',
                        'Injuries': '19.0',
                        'Communicable & other Group I': '23.6'
                    },
                    'Latvia': {
                        'Noncommunicable diseases': '623.7',
                        'Injuries': '54.5',
                        'Communicable & other Group I': '26.0'
                    },
                    'Turkmenistan': {
                        'Injuries': '93.0',
                        'Communicable & other Group I': '115.8',
                        'Noncommunicable diseases': '1025.1'
                    },
                    'Switzerland': {
                        'Communicable & other Group I': '14.5',
                        'Noncommunicable diseases': '291.6',
                        'Injuries': '25.4'
                    },
                    'Luxembourg': {
                        'Injuries': '31.1',
                        'Noncommunicable diseases': '317.8',
                        'Communicable & other Group I': '20.5'
                    },
                    'Georgia': {
                        'Injuries': '32.2',
                        'Communicable & other Group I': '39.3',
                        'Noncommunicable diseases': '615.2'
                    },
                    'Slovenia': {
                        'Noncommunicable diseases': '369.2',
                        'Communicable & other Group I': '15.4',
                        'Injuries': '44.2'
                    },
                    'Montenegro': {
                        'Communicable & other Group I': '18.7',
                        'Noncommunicable diseases': '571.5',
                        'Injuries': '41.2'
                    },
                    'Armenia': {
                        'Noncommunicable diseases': '847.5',
                        'Communicable & other Group I': '45.0',
                        'Injuries': '49.2'
                    },
                    'Germany': {
                        'Injuries': '23.0',
                        'Communicable & other Group I': '21.6',
                        'Noncommunicable diseases': '365.1'
                    },
                    'Czech Republic': {
                        'Injuries': '39.1',
                        'Noncommunicable diseases': '460.7',
                        'Communicable & other Group I': '27.0'
                    }
                },
                'Africa': {
                    'Equatorial Guinea': {
                        'Communicable & other Group I': '756.8',
                        'Injuries': '133.6',
                        'Noncommunicable diseases': '729.0'
                    },
                    'Madagascar': {
                        'Noncommunicable diseases': '648.6',
                        'Communicable & other Group I': '429.9',
                        'Injuries': '89.0'
                    },
                    'Swaziland': {
                        'Communicable & other Group I': '884.3',
                        'Injuries': '119.5',
                        'Noncommunicable diseases': '702.4'
                    },
                    'Congo': {
                        'Noncommunicable diseases': '632.3',
                        'Communicable & other Group I': '666.9',
                        'Injuries': '89.0'
                    },
                    'Burkina Faso': {
                        'Communicable & other Group I': '648.2',
                        'Noncommunicable diseases': '784.0',
                        'Injuries': '119.3'
                    },
                    'Guinea-Bissau': {
                        'Communicable & other Group I': '869.8',
                        'Noncommunicable diseases': '764.7',
                        'Injuries': '111.6'
                    },
                    'Democratic Republic of the Congo': {
                        'Noncommunicable diseases': '724.4',
                        'Injuries': '137.1',
                        'Communicable & other Group I': '920.7'
                    },
                    'Mozambique': {
                        'Injuries': '175.3',
                        'Noncommunicable diseases': '593.7',
                        'Communicable & other Group I': '998.1'
                    },
                    'Central African Republic': {
                        'Communicable & other Group I': '1212.1',
                        'Injuries': '107.9',
                        'Noncommunicable diseases': '550.8'
                    },
                    'United Republic of Tanzania': {
                        'Noncommunicable diseases': '569.8',
                        'Communicable & other Group I': '584.2',
                        'Injuries': '129.2'
                    },
                    'Cameroon': {
                        'Communicable & other Group I': '768.8',
                        'Injuries': '106.0',
                        'Noncommunicable diseases': '675.2'
                    },
                    'Togo': {
                        'Noncommunicable diseases': '679.0',
                        'Communicable & other Group I': '681.8',
                        'Injuries': '93.0'
                    },
                    'Eritrea': {
                        'Injuries': '118.7',
                        'Communicable & other Group I': '506.0',
                        'Noncommunicable diseases': '671.9'
                    },
                    'Namibia': {
                        'Injuries': '76.4',
                        'Noncommunicable diseases': '580.2',
                        'Communicable & other Group I': '356.6'
                    },
                    'Senegal': {
                        'Noncommunicable diseases': '558.1',
                        'Injuries': '89.3',
                        'Communicable & other Group I': '587.7'
                    },
                    'Chad': {
                        'Communicable & other Group I': '1070.9',
                        'Injuries': '114.5',
                        'Noncommunicable diseases': '712.6'
                    },
                    'Benin': {
                        'Injuries': '98.0',
                        'Noncommunicable diseases': '761.5',
                        'Communicable & other Group I': '577.3'
                    },
                    'Zimbabwe': {
                        'Communicable & other Group I': '711.3',
                        'Injuries': '82.5',
                        'Noncommunicable diseases': '598.9'
                    },
                    'Rwanda': {
                        'Noncommunicable diseases': '585.3',
                        'Injuries': '106.3',
                        'Communicable & other Group I': '401.7'
                    },
                    'Zambia': {
                        'Noncommunicable diseases': '587.4',
                        'Injuries': '156.4',
                        'Communicable & other Group I': '764.3'
                    },
                    'Mali': {
                        'Injuries': '119.5',
                        'Communicable & other Group I': '588.3',
                        'Noncommunicable diseases': '866.1'
                    },
                    'Ethiopia': {
                        'Injuries': '94.5',
                        'Communicable & other Group I': '558.9',
                        'Noncommunicable diseases': '476.3'
                    },
                    'South Africa': {
                        'Communicable & other Group I': '611.6',
                        'Injuries': '103.5',
                        'Noncommunicable diseases': '710.9'
                    },
                    'Burundi': {
                        'Injuries': '146.6',
                        'Communicable & other Group I': '704.8',
                        'Noncommunicable diseases': '729.5'
                    },
                    'Cabo Verde': {
                        'Injuries': '54.4',
                        'Noncommunicable diseases': '482.1',
                        'Communicable & other Group I': '141.9'
                    },
                    'Liberia': {
                        'Noncommunicable diseases': '656.9',
                        'Injuries': '83.3',
                        'Communicable & other Group I': '609.1'
                    },
                    'Uganda': {
                        'Noncommunicable diseases': '664.4',
                        'Communicable & other Group I': '696.7',
                        'Injuries': '166.8'
                    },
                    'Mauritius': {
                        'Noncommunicable diseases': '576.5',
                        'Injuries': '44.1',
                        'Communicable & other Group I': '61.8'
                    },
                    'Algeria': {
                        'Noncommunicable diseases': '710.4',
                        'Injuries': '53.8',
                        'Communicable & other Group I': '97.8'
                    },
                    'C\u00f4te d\'Ivoire': {
                        'Noncommunicable diseases': '794.0',
                        'Injuries': '124.0',
                        'Communicable & other Group I': '861.3'
                    },
                    'Malawi': {
                        'Injuries': '97.7',
                        'Communicable & other Group I': '777.6',
                        'Noncommunicable diseases': '655.0'
                    },
                    'Botswana': {
                        'Injuries': '87.9',
                        'Noncommunicable diseases': '612.2',
                        'Communicable & other Group I': '555.3'
                    },
                    'Guinea': {
                        'Injuries': '96.0',
                        'Noncommunicable diseases': '681.1',
                        'Communicable & other Group I': '679.6'
                    },
                    'Ghana': {
                        'Injuries': '76.1',
                        'Noncommunicable diseases': '669.9',
                        'Communicable & other Group I': '476.0'
                    },
                    'Kenya': {
                        'Noncommunicable diseases': '514.7',
                        'Injuries': '101.1',
                        'Communicable & other Group I': '657.5'
                    },
                    'Gambia': {
                        'Noncommunicable diseases': '629.6',
                        'Injuries': '96.0',
                        'Communicable & other Group I': '590.5'
                    },
                    'Angola': {
                        'Injuries': '137.8',
                        'Noncommunicable diseases': '768.4',
                        'Communicable & other Group I': '873.3'
                    },
                    'Sierra Leone': {
                        'Communicable & other Group I': '1327.4',
                        'Noncommunicable diseases': '963.5',
                        'Injuries': '149.5'
                    },
                    'Mauritania': {
                        'Communicable & other Group I': '619.1',
                        'Injuries': '83.4',
                        'Noncommunicable diseases': '555.1'
                    },
                    'Comoros': {
                        'Communicable & other Group I': '494.6',
                        'Injuries': '132.4',
                        'Noncommunicable diseases': '695.5'
                    },
                    'Gabon': {
                        'Noncommunicable diseases': '504.6',
                        'Injuries': '77.4',
                        'Communicable & other Group I': '589.4'
                    },
                    'Niger': {
                        'Injuries': '97.6',
                        'Communicable & other Group I': '740.0',
                        'Noncommunicable diseases': '649.1'
                    },
                    'Lesotho': {
                        'Communicable & other Group I': '1110.5',
                        'Injuries': '142.5',
                        'Noncommunicable diseases': '671.8'
                    },
                    'Nigeria': {
                        'Noncommunicable diseases': '673.7',
                        'Communicable & other Group I': '866.2',
                        'Injuries': '145.6'
                    }
                },
                'Americas': {
                    'Canada': {
                        'Noncommunicable diseases': '318.0',
                        'Injuries': '31.3',
                        'Communicable & other Group I': '22.6'
                    },
                    'Bolivia (Plurinational State of)': {
                        'Communicable & other Group I': '226.2',
                        'Noncommunicable diseases': '635.3',
                        'Injuries': '100.0'
                    },
                    'Haiti': {
                        'Communicable & other Group I': '405.4',
                        'Noncommunicable diseases': '724.6',
                        'Injuries': '89.3'
                    },
                    'Belize': {
                        'Noncommunicable diseases': '470.7',
                        'Injuries': '82.0',
                        'Communicable & other Group I': '104.6'
                    },
                    'Suriname': {
                        'Injuries': '70.5',
                        'Communicable & other Group I': '83.7',
                        'Noncommunicable diseases': '374.8'
                    },
                    'Argentina': {
                        'Communicable & other Group I': '68.7',
                        'Injuries': '50.7',
                        'Noncommunicable diseases': '467.3'
                    },
                    'Mexico': {
                        'Injuries': '63.2',
                        'Communicable & other Group I': '57.0',
                        'Noncommunicable diseases': '468.3'
                    },
                    'Jamaica': {
                        'Injuries': '51.5',
                        'Communicable & other Group I': '97.0',
                        'Noncommunicable diseases': '519.1'
                    },
                    'Peru': {
                        'Noncommunicable diseases': '363.5',
                        'Injuries': '47.9',
                        'Communicable & other Group I': '121.3'
                    },
                    'Brazil': {
                        'Injuries': '80.2',
                        'Communicable & other Group I': '92.8',
                        'Noncommunicable diseases': '513.8'
                    },
                    'Venezuela (Bolivarian Republic of)': {
                        'Communicable & other Group I': '58.2',
                        'Injuries': '103.2',
                        'Noncommunicable diseases': '410.6'
                    },
                    'Paraguay': {
                        'Noncommunicable diseases': '485.5',
                        'Communicable & other Group I': '77.3',
                        'Injuries': '67.6'
                    },
                    'Chile': {
                        'Noncommunicable diseases': '366.5',
                        'Communicable & other Group I': '36.3',
                        'Injuries': '41.2'
                    },
                    'Trinidad and Tobago': {
                        'Noncommunicable diseases': '705.3',
                        'Communicable & other Group I': '80.4',
                        'Injuries': '98.4'
                    },
                    'Colombia': {
                        'Noncommunicable diseases': '377.3',
                        'Communicable & other Group I': '55.0',
                        'Injuries': '72.6'
                    },
                    'Cuba': {
                        'Injuries': '45.3',
                        'Noncommunicable diseases': '421.8',
                        'Communicable & other Group I': '33.2'
                    },
                    'El Salvador': {
                        'Noncommunicable diseases': '474.9',
                        'Injuries': '157.7',
                        'Communicable & other Group I': '96.2'
                    },
                    'Honduras': {
                        'Injuries': '80.8',
                        'Communicable & other Group I': '117.5',
                        'Noncommunicable diseases': '441.5'
                    },
                    'Ecuador': {
                        'Noncommunicable diseases': '409.7',
                        'Injuries': '83.7',
                        'Communicable & other Group I': '97.3'
                    },
                    'Costa Rica': {
                        'Communicable & other Group I': '30.5',
                        'Noncommunicable diseases': '391.8',
                        'Injuries': '46.5'
                    },
                    'Dominican Republic': {
                        'Noncommunicable diseases': '396.0',
                        'Injuries': '66.4',
                        'Communicable & other Group I': '76.8'
                    },
                    'Nicaragua': {
                        'Communicable & other Group I': '75.2',
                        'Injuries': '64.4',
                        'Noncommunicable diseases': '546.6'
                    },
                    'Barbados': {
                        'Noncommunicable diseases': '404.5',
                        'Injuries': '28.0',
                        'Communicable & other Group I': '60.8'
                    },
                    'Uruguay': {
                        'Noncommunicable diseases': '446.0',
                        'Injuries': '53.8',
                        'Communicable & other Group I': '46.2'
                    },
                    'Panama': {
                        'Communicable & other Group I': '86.1',
                        'Injuries': '67.4',
                        'Noncommunicable diseases': '372.9'
                    },
                    'Bahamas': {
                        'Noncommunicable diseases': '465.2',
                        'Injuries': '45.7',
                        'Communicable & other Group I': '122.0'
                    },
                    'Guyana': {
                        'Communicable & other Group I': '177.2',
                        'Noncommunicable diseases': '1024.2',
                        'Injuries': '150.0'
                    },
                    'United States of America': {
                        'Noncommunicable diseases': '412.8',
                        'Injuries': '44.2',
                        'Communicable & other Group I': '31.3'
                    },
                    'Guatemala': {
                        'Communicable & other Group I': '212.7',
                        'Noncommunicable diseases': '409.4',
                        'Injuries': '111.0'
                    }
                },
                'Eastern Mediterranean': {
                    'Egypt': {
                        'Communicable & other Group I': '74.3',
                        'Noncommunicable diseases': '781.7',
                        'Injuries': '33.5'
                    },
                    'South Sudan': {
                        'Injuries': '143.4',
                        'Communicable & other Group I': '831.3',
                        'Noncommunicable diseases': '623.4'
                    },
                    'Sudan': {
                        'Injuries': '133.6',
                        'Noncommunicable diseases': '551.0',
                        'Communicable & other Group I': '495.0'
                    },
                    'Libya': {
                        'Injuries': '62.8',
                        'Noncommunicable diseases': '550.0',
                        'Communicable & other Group I': '52.6'
                    },
                    'Jordan': {
                        'Noncommunicable diseases': '640.3',
                        'Injuries': '53.5',
                        'Communicable & other Group I': '52.5'
                    },
                    'Pakistan': {
                        'Communicable & other Group I': '296.0',
                        'Noncommunicable diseases': '669.3',
                        'Injuries': '98.7'
                    },
                    'Djibouti': {
                        'Noncommunicable diseases': '631.1',
                        'Communicable & other Group I': '626.0',
                        'Injuries': '106.0'
                    },
                    'Syrian Arab Republic': {
                        'Communicable & other Group I': '41.0',
                        'Injuries': '308.0',
                        'Noncommunicable diseases': '572.7'
                    },
                    'Morocco': {
                        'Noncommunicable diseases': '707.7',
                        'Communicable & other Group I': '131.5',
                        'Injuries': '47.0'
                    },
                    'Yemen': {
                        'Communicable & other Group I': '515.0',
                        'Noncommunicable diseases': '626.9',
                        'Injuries': '84.3'
                    },
                    'Bahrain': {
                        'Injuries': '33.5',
                        'Noncommunicable diseases': '505.7',
                        'Communicable & other Group I': '48.5'
                    },
                    'United Arab Emirates': {
                        'Noncommunicable diseases': '546.8',
                        'Injuries': '31.5',
                        'Communicable & other Group I': '35.6'
                    },
                    'Lebanon': {
                        'Noncommunicable diseases': '384.6',
                        'Injuries': '40.6',
                        'Communicable & other Group I': '30.5'
                    },
                    'Saudi Arabia': {
                        'Noncommunicable diseases': '549.4',
                        'Injuries': '41.1',
                        'Communicable & other Group I': '71.3'
                    },
                    'Iran (Islamic Republic of)': {
                        'Injuries': '74.9',
                        'Communicable & other Group I': '56.2',
                        'Noncommunicable diseases': '569.3'
                    },
                    'Iraq': {
                        'Communicable & other Group I': '87.0',
                        'Noncommunicable diseases': '715.5',
                        'Injuries': '128.5'
                    },
                    'Qatar': {
                        'Communicable & other Group I': '28.3',
                        'Injuries': '41.0',
                        'Noncommunicable diseases': '407.0'
                    },
                    'Afghanistan': {
                        'Communicable & other Group I': '362.7',
                        'Injuries': '169.2',
                        'Noncommunicable diseases': '846.3'
                    },
                    'Somalia': {
                        'Noncommunicable diseases': '550.7',
                        'Communicable & other Group I': '927.2',
                        'Injuries': '188.5'
                    },
                    'Kuwait': {
                        'Communicable & other Group I': '82.5',
                        'Injuries': '25.4',
                        'Noncommunicable diseases': '406.3'
                    },
                    'Oman': {
                        'Injuries': '52.8',
                        'Noncommunicable diseases': '478.2',
                        'Communicable & other Group I': '84.2'
                    },
                    'Tunisia': {
                        'Noncommunicable diseases': '509.3',
                        'Communicable & other Group I': '65.0',
                        'Injuries': '39.1'
                    }
                },
                'Western Pacific': {
                    'Mongolia': {
                        'Injuries': '69.4',
                        'Noncommunicable diseases': '966.5',
                        'Communicable & other Group I': '82.8'
                    },
                    'Cambodia': {
                        'Injuries': '62.2',
                        'Communicable & other Group I': '227.5',
                        'Noncommunicable diseases': '394.0'
                    },
                    'Japan': {
                        'Injuries': '40.5',
                        'Noncommunicable diseases': '244.2',
                        'Communicable & other Group I': '33.9'
                    },
                    'Brunei Darussalam': {
                        'Injuries': '44.6',
                        'Noncommunicable diseases': '475.3',
                        'Communicable & other Group I': '56.1'
                    },
                    'Solomon Islands': {
                        'Communicable & other Group I': '230.6',
                        'Injuries': '75.1',
                        'Noncommunicable diseases': '709.7'
                    },
                    'Viet Nam': {
                        'Communicable & other Group I': '96.0',
                        'Injuries': '59.0',
                        'Noncommunicable diseases': '435.4'
                    },
                    'Lao People\'s Democratic Republic': {
                        'Communicable & other Group I': '328.7',
                        'Injuries': '75.2',
                        'Noncommunicable diseases': '680.0'
                    },
                    'China': {
                        'Communicable & other Group I': '41.4',
                        'Noncommunicable diseases': '576.3',
                        'Injuries': '50.4'
                    },
                    'New Zealand': {
                        'Injuries': '32.9',
                        'Noncommunicable diseases': '313.6',
                        'Communicable & other Group I': '18.0'
                    },
                    'Papua New Guinea': {
                        'Injuries': '100.1',
                        'Communicable & other Group I': '554.3',
                        'Noncommunicable diseases': '693.2'
                    },
                    'Philippines': {
                        'Communicable & other Group I': '226.4',
                        'Noncommunicable diseases': '720.0',
                        'Injuries': '53.8'
                    },
                    'Malaysia': {
                        'Injuries': '62.8',
                        'Noncommunicable diseases': '563.2',
                        'Communicable & other Group I': '117.4'
                    },
                    'Australia': {
                        'Communicable & other Group I': '13.7',
                        'Noncommunicable diseases': '302.9',
                        'Injuries': '28.2'
                    },
                    'Fiji': {
                        'Noncommunicable diseases': '804.0',
                        'Injuries': '64.0',
                        'Communicable & other Group I': '105.2'
                    },
                    'Singapore': {
                        'Communicable & other Group I': '66.2',
                        'Noncommunicable diseases': '264.8',
                        'Injuries': '17.5'
                    },
                    'Republic of Korea': {
                        'Injuries': '53.1',
                        'Communicable & other Group I': '33.8',
                        'Noncommunicable diseases': '302.1'
                    }
                }
            },
            points = [],
            regionP,
            regionVal,
            regionI = 0,
            countryP,
            countryI,
            causeP,
            causeI,
            region,
            country,
            cause,
            causeName = {
                'Communicable & other Group I': 'Communicable diseases',
                'Noncommunicable diseases': 'Non-communicable diseases',
                'Injuries': 'Injuries'
            };

        for (region in data11) {
            if (data11.hasOwnProperty(region)) {
                regionVal = 0;
                regionP = {
                    id: 'id_' + regionI,
                    name: region,
                    color: Highcharts.getOptions().colors[regionI]
                };
                countryI = 0;
                for (country in data11[region]) {
                    if (data11[region].hasOwnProperty(country)) {
                        countryP = {
                            id: regionP.id + '_' + countryI,
                            name: country,
                            parent: regionP.id
                        };
                        points.push(countryP);
                        causeI = 0;
                        for (cause in data11[region][country]) {
                            if (data11[region][country].hasOwnProperty(cause)) {
                                causeP = {
                                    id: countryP.id + '_' + causeI,
                                    name: causeName[cause],
                                    parent: countryP.id,
                                    value: Math.round(+data11[region][country][cause])
                                };
                                regionVal += causeP.value;
                                points.push(causeP);
                                causeI = causeI + 1;
                            }
                        }
                        countryI = countryI + 1;
                    }
                }
                regionP.value = Math.round(regionVal / countryI);
                points.push(regionP);
                regionI = regionI + 1;
            }
        }
        Highcharts.chart('tree', {
            responsive: responsiveData,
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                allowDrillToNode: true,
                animationLimit: 1000,
                dataLabels: {
                    enabled: false,
                    color: '#000',
                    style: {
                        textOutline: false,
                        fontSize: "10px",
                        color: "#000",
                        fontWeight: 'light',
                        padding: 15,
                    }
                },
                levelIsConstant: false,
                levels: [{
                    level: 1,
                    dataLabels: {
                        enabled: true
                    },
                    borderWidth: 3
                }],
                data: points
            }],
            subtitle: {
                text: ''
            },
            title: {
                text: ''
            }
        });
    }
    if ($('#pyramid1').length > 0) {
        Highcharts.chart('pyramid1', {
            responsive: responsiveData,
            chart: {
                type: 'pyramid'
            },
            title: {
                text: '',
                x: -50
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                        softConnector: true
                    },
                    center: ['50%', '50%'],
                    width: '60%'
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Unique users',
                data: [
                    ['Website', 100],
                    ['Website visits', 200],
                    ['Downloads', 200],
                    ['Requested price list', 200],
                    ['Invoice sent', 200],
                    ['Finalized', 100]
                ]
            }]
        });
    }
    if ($('#3D-bar').length > 0) {
        var chart = new Highcharts.Chart({
            responsive: responsiveData,
            chart: {
                renderTo: '3D-bar',
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                column: {
                    depth: 25,
                    lineWidth: 1
                }
            },
            series: [{
                data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
                color: Highcharts.getOptions().colors[7]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            yAxis: {
                lineWidth: 1,
                //gridLineWidth:0,

            },
            xAxis: {
                lineWidth: 1,
                //gridLineWidth:0,

            }
        });
    }
    if ($('#dashboard-line-bar').length > 0) {
        Highcharts.chart('dashboard-line-bar', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft: 40,
                marginTop: 20,
                marginBottom: 55,
                padding: 30,
                spacing: 0,
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ],
                crosshair: true,
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    },
                    enabled: false,
                }
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    enabled: false,
                    format: '{value}°C',
                    style: {
                        color: Highcharts.getOptions().colors[7]
                    }
                },
                title: {
                    enabled: false,
                    text: 'Temperature',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 0,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    enabled: false,
                    style: {
                        color: '#000000'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Rainfall',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} mm',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true,
                gridLineWidth: 0,
                visible: false
            }],
            tooltip: {
                enabled: false,
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    borderColor: '#ff0000',
                    pointWidth: 16
                },
                showInLegend: true
            },
            series: [{
                name: 'Rainfall',
                type: 'column',
                color: Highcharts.getOptions().colors[8],
                yAxis: 1,
                data: [49.9, 106.5, 71.4],
                tooltip: {
                    valueSuffix: ' mm'
                }

            }, {
                name: 'Temperature',
                type: 'spline',
                color: Highcharts.getOptions().colors[11],
                data: [20.8, 50.9, 29],
                tooltip: {
                    valueSuffix: '°C'
                }
            }],
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -20,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
        });
    }
    if ($('#dashboard-line-bar1').length > 0) {
        Highcharts.chart('dashboard-line-bar1', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                marginLeft: 40,
                marginTop: 20,
                marginBottom: 55,
                padding: 30,
                spacing: 0,
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                ],
                crosshair: true,
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    },
                    enabled: false,
                }
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    enabled: false,
                    format: '{value}°C',
                    style: {
                        color: Highcharts.getOptions().colors[7]
                    }
                },
                title: {
                    enabled: false,
                    text: 'Temperature',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 0,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    enabled: false,
                    style: {
                        color: '#000000'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Rainfall',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} mm',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true,
                gridLineWidth: 0,
                visible: false
            }],
            tooltip: {
                enabled: false,
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    borderColor: '#ff0000',
                    pointWidth: 16
                },
                showInLegend: true
            },
            series: [{
                name: 'Rainfall',
                type: 'column',
                color: Highcharts.getOptions().colors[8],
                yAxis: 1,
                data: [49.9, 106.5, 71.4],
                tooltip: {
                    valueSuffix: ' mm'
                }

            }, {
                name: 'Temperature',
                type: 'spline',
                color: Highcharts.getOptions().colors[11],
                data: [20.8, 50.9, 29],
                tooltip: {
                    valueSuffix: '°C'
                }
            }],
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -20,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
        });
    }
    if ($('#container11').length > 0) {
        Highcharts.chart('container11', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'option1',
                    color: Highcharts.getOptions().colors[13],
                    y: 45
                }, {
                    name: 'option2',
                    color: Highcharts.getOptions().colors[12],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'option3',
                    color: Highcharts.getOptions().colors[15],
                    y: 10
                }, {
                    name: 'option4',
                    color: Highcharts.getOptions().colors[14],
                    y: 5
                }]
            }]
        });
    }
    if ($('#container12').length > 0) {
        Highcharts.chart('container12', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'option1',
                    color: Highcharts.getOptions().colors[13],
                    y: 45
                }, {
                    name: 'option2',
                    color: Highcharts.getOptions().colors[12],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'option3',
                    color: Highcharts.getOptions().colors[15],
                    y: 10
                }, {
                    name: 'option4',
                    color: Highcharts.getOptions().colors[14],
                    y: 5
                }]
            }]
        });
    }
    if ($('#container13').length > 0) {
        Highcharts.chart('container13', {
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1200
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }, {
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 132,
                            width: 150
                        }
                    }
                }]
            },
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false,
                margin: 25,
                padding: 30,
                spacing: 0,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: false
            },
            legend: {
                markerMargin: 3,
                align: 'right',
                verticalAlign: 'bottom',
                itemWrap: false,
                x: -10,
                y: -15,
                itemMarginBottom: 3,
                itemStyle: {
                    color: '#666666',
                    fontWeight: 'light',
                    fontSize: '9px',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
                        distance: 0,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['70%', '0%'],
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'option1',
                    color: Highcharts.getOptions().colors[13],
                    y: 45
                }, {
                    name: 'option2',
                    color: Highcharts.getOptions().colors[12],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'option3',
                    color: Highcharts.getOptions().colors[15],
                    y: 10
                }, {
                    name: 'option4',
                    color: Highcharts.getOptions().colors[14],
                    y: 5
                }]
            }]
        });
    }
}, 2000);

function redrawOnModal() {
    if ($('#stacked-column-modal').length > 0) {
        Highcharts.chart('stacked-column-modal', {
            chart: {
                type: 'column',
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: ['01', '02', '03', '04', '05', '06', '07'],
                minPadding: 50,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Users  ( in Millions )',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            legend: legends,
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    borderWidth: 0,
                },
                series: {
                    borderWidth: 0,
                    pointWidth: 14,

                }
            },
            series: [{
                    name: 'Joe',
                    color: Highcharts.getOptions().colors[5],
                    data: [0.2, 0.2, 0.2, 0.5, 0.65, 0.5, 0.25]
                },
                {
                    name: 'John',
                    color: Highcharts.getOptions().colors[4],
                    data: [0.2, 0.15, 0.4, 0.2, 0.15, 0.25, 0.2]
                }, {
                    name: 'Jane',
                    color: Highcharts.getOptions().colors[3],
                    data: [0.3, 0.2, 0.2, 0.1, 0.1, 0.02, 0.25]
                }, {
                    name: 'Joe',
                    color: Highcharts.getOptions().colors[2],
                    data: [0.25, 0.1, 0.5, 0.15, 0.2, 0.2, 0.2]
                },
                {
                    name: 'John',
                    color: Highcharts.getOptions().colors[1],
                    data: [0.25, 0.25, 0.05, 0.1, 0.2, 0.2, 0.2]
                }, {
                    name: 'Jane',
                    color: Highcharts.getOptions().colors[0],
                    data: [0.15, 0.40, 0.40, 0.50, 0.25, 0.25, 0.6]
                }
            ]
        });
    };
    if ($('#stacked-column1-modal').length > 0) {
        Highcharts.chart('stacked-column1-modal', {
            chart: {
                type: 'column',
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: ['01', '02', '03', '04', '05', '06', '07'],
                minPadding: 50,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Users  ( in Millions )',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            legend: legends,
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#000"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    borderWidth: 0
                },
                series: {
                    borderWidth: 0,
                    pointWidth: 14,
                    //colors:Highcharts.getOptions().colors.reverse(),
                }
            },
            series: [{
                    name: 'John',
                    //color: '#a3ce76',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                }, {
                    name: 'Joe',
                    //color: '#cf6d84',
                    data: [5, 3, 4, 7, 2, 1, 2]
                }, {
                    name: 'Joe',
                    //color: '#e5d076',
                    data: [5, 3, 4, 7, 2, 2, 3]
                }, {
                    name: 'Joe',
                    //color: '#a4ce76',
                    data: [5, 3, 4, 7, 2, 5, 1]
                }, {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                }, {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                },
                {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a583a5',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }, {
                    name: 'Joe',
                    //color: '#5fb6b8',
                    data: [4, 7, 2, 5, 3, 4, 1]
                },
                {
                    name: 'John',
                    //color: '#f4a289',
                    data: [5, 3, 4, 7, 2, 1, 3]
                }, {
                    name: 'Jane',
                    //color: '#a3ce76',
                    data: [3, 4, 7, 1, 5, 5, 2]
                }
            ]
        });
    };
    if ($('#stacked-bar-modal').length > 0) {
        Highcharts.chart('stacked-bar-modal', {
            responsive: responsiveData,
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes'],
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total fruit consumption'
                },
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
            },
            legend: legends,
            plotOptions: {
                series: {
                    stacking: 'normal',
                    pointWidth: 18,
                    //pointPadding: 0,
                    //groupPadding:0.0
                }
            },
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7],
                color: Highcharts.getOptions().colors[15],
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2],
                color: Highcharts.getOptions().colors[4],
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2],
                color: Highcharts.getOptions().colors[11],
            }]
        });
    }
    if ($('#bar-graph-modal').length > 0) {
        Highcharts.chart('bar-graph-modal', {
            chart: {
                type: 'column'
            },
            responsive: responsiveData,
            title: {
                text: ''
            },
            legend: legends,
            subtitle: {
                text: ''
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                categories: [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7'
                ],
                crosshair: true,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                min: 0,
                lineWidth: 1,
                title: {
                    text: 'Rainfall (mm)',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    pointWidth: 18,
                    borderRadius: 5,
                }
            },
            series: [{
                name: 'Tokyo',
                color: Highcharts.getOptions().colors[12], //'#a4ce76',
                data: [1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25]

            }]
        });
    };
    if ($('#line-graph-modal').length > 0) {
        Highcharts.chart('line-graph-modal', {
            responsive: responsiveData,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineWidth: 1,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                },
            },
            series: [{
                name: 'Installation',
                //color: '#e5d076',
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
        });
    };
    if ($('#line-graph2-modal').length > 0) {
        Highcharts.chart('line-graph2-modal', {
            responsive: responsiveData,
            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },
            yAxis: {
                lineWidth: 1,
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                title: {
                    text: 'No. of Days Played',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },
            series: [{
                name: 'Installation',
                color: Highcharts.getOptions().colors[1],
                data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
            }, {
                name: 'Manufacturing',
                //color: '#cf6d84',
                color: Highcharts.getOptions().colors[2],
                data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }

            // responsive: {
            //     rules: [{
            //         condition: {
            //             maxWidth: 500
            //         },
            //         chartOptions: {
            //             chart: {
            //                 height: 200
            //             },
            //             legend: {
            //                 layout: 'horizontal',
            //                 align: 'center',
            //                 verticalAlign: 'bottom'
            //             }
            //         }
            //     }]
            // }

        });
    };
    if ($('#frequency-curve-modal').length > 0) {
        Highcharts.chart('frequency-curve-modal', {
            responsive: responsiveData,
            chart: {
                type: 'spline'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date',
                    style: {
                        color: "#000000"
                    }
                },
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Snow depth (m)',
                    style: {
                        color: "#000000"
                    }
                },
                min: 0,
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [{
                name: 'Winter 2012-2013',
                //color: '#a4ce76',
                // Define the data points. All series have a dummy year
                // of 1970/71 in order to be compared on the same x axis. Note
                // that in JavaScript, months start at 0 for January, 1 for February etc.
                data: [
                    [Date.UTC(1970, 9, 29), 0.5],
                    [Date.UTC(1970, 10, 9), 1.5],
                    [Date.UTC(1970, 11, 1), 1.0],
                    [Date.UTC(1971, 0, 1), 0.8],
                    [Date.UTC(1971, 0, 16), 0.7],
                    [Date.UTC(1971, 1, 19), 0.75],
                    [Date.UTC(1971, 2, 25), 0.4]
                ]
            }],
            legend: legends
        });
    }
    if ($('#frequency-polygon-modal').length > 0) {
        Highcharts.chart('frequency-polygon-modal', {
            responsive: responsiveData,
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            yAxis: {
                title: {
                    text: 'Number of Employees',
                    style: {
                        color: "#000000"
                    }
                },
                lineWidth: 1,
                gridLineWidth: 0,
                lineColor: "#ebebeb",
                labels: {
                    style: {
                        color: '#000000'
                    }
                }
            },
            xAxis: {
                gridLineWidth: 0,
                tickLength: 0,
                lineColor: "#ebebeb",
                title: {
                    style: {
                        color: "#000000"
                    }
                },
                labels: {
                    style: {
                        color: '#000000'
                    }
                },
                lineWidth: 1,
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },
            series: [{
                name: 'Installation',
                //color: '#e5d076',
                data: [43934, 57177, 43934, 69658, 43934, 119931, 53934, 154175]
            }],
            legend: legends,
            tooltip: {
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            }
            // responsive: {
            //     rules: [{
            //         condition: {
            //             maxWidth: 500
            //         },
            //         chartOptions: {
            //             legend: {
            //                 layout: 'horizontal',
            //                 align: 'center',
            //                 verticalAlign: 'bottom'
            //             }
            //         }
            //     }]
            // }

        });
    }
    if ($('#pie-chart-modal').length > 0) {
        Highcharts.chart('pie-chart-modal', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            color: '#000000',
                            fontWeight: "bold",
                            borderColor: "transparent"
                        }
                    },
                    showInLegend: true
                }
            },
            legend: legends,
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[15],
                    y: 45
                }, {
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[16],
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[17],
                    y: 10
                }, {
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[14],
                    y: 5
                }, {
                    name: '(479,219) ',
                    color: Highcharts.getOptions().colors[13],
                    y: 15
                }]
            }],

        });
    }
    if ($('#pie-chart2-modal').length > 0) {
        Highcharts.chart('pie-chart2-modal', {
            responsive: responsiveData,
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '',
                backgroundColor: "#fff",
                borderWidth: 0,
                shape: "none",
                style: {
                    fontSize: "13px",
                    color: "#666666"
                },
                padding: 15,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: -5,
                        format: '{point.name}: {point.percentage:.1f} %',
                        style: {
                            fontSize: "8",
                            color: 'black',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                            style: {
                                fontWeight: 'bold',
                                color: '#000000',
                                borderColor: "transparent",
                            }
                        }
                    },
                    startAngle: 0,
                    endAngle: 360,
                    center: ['50%', '50%']
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'a',
                    //color: '#e4cf75',
                    y: 45
                }, {
                    name: 'b',
                    //color: '#a483a4',
                    y: 25,
                    // sliced: true,
                    // selected: true
                }, {
                    name: 'c',
                    //color: '#5fb6b8',
                    y: 10
                }, {
                    name: 'd',
                    //color: '#cf6c83',
                    y: 5
                }, {
                    name: 'e',
                    //color: '#a3ce76',
                    y: 15
                }]
            }]
        });
    }
}





// function showValues() {
//     $('#alpha-value').html(chart.options.chart.options3d.alpha);
//     $('#beta-value').html(chart.options.chart.options3d.beta);
//     $('#depth-value').html(chart.options.chart.options3d.depth);
// }


// $('#sliders input').on('input change', function() {
//     chart.options.chart.options3d[this.id] = parseFloat(this.value);
//     showValues();
//     chart.redraw(false);
// });

// showValues();